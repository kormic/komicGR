process.env.NODE_ENV = 'test';
process.env.confirmEmailSecret = 'test';
process.env.TS_NODE_PROJECT = './tsconfig.spec.json';
process.env.TS_NODE_LOG_ERROR = true;

module.exports = {
  extension: ['ts'],
  spec: './src/Test/**/*.spec.ts',
  require: [
    'reflect-metadata/Reflect',
    'ts-node/register',
    'tsconfig-paths/register',
    'dotenv/config'
  ],
  watch: true,
  'watch-files': ['**.ts'],
  recursive: true
};
