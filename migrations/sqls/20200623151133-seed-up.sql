-- MariaDB dump 10.17  Distrib 10.4.4-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: komicgr
-- ------------------------------------------------------
-- Server version	10.4.4-MariaDB-1:10.4.4+maria~bionic

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories`
(
  `id` int
(11) NOT NULL AUTO_INCREMENT,
  `name` varchar
(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY
(`id`),
  UNIQUE KEY `categories_UN`
(`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `posts`
--

DROP TABLE IF EXISTS `posts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `posts`
(
  `id` int
(11) NOT NULL AUTO_INCREMENT,
  `title` varchar
(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `short_body` varchar
(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `likes` int
(11) DEFAULT 0,
  `user_id` int
(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `image_url` varchar
(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY
(`id`),
  KEY `posts_users_FK`
(`user_id`),
  CONSTRAINT `posts_users_FK` FOREIGN KEY
(`user_id`) REFERENCES `users`
(`id`) ON
DELETE
SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=144 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `posts_categories`
--

DROP TABLE IF EXISTS `posts_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `posts_categories`
(
  `post_id` int
(11) NOT NULL,
  `category_id` int
(11) NOT NULL,
  PRIMARY KEY
(`post_id`,`category_id`),
  KEY `Posts_Categories_Categories_FK`
(`category_id`),
  CONSTRAINT `posts_categories_categories_FK` FOREIGN KEY
(`category_id`) REFERENCES `categories`
(`Id`),
  CONSTRAINT `posts_categories_posts_FK` FOREIGN KEY
(`post_id`) REFERENCES `posts`
(`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `posts_tags`
--

DROP TABLE IF EXISTS `posts_tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `posts_tags`
(
  `post_id` int
(11) NOT NULL,
  `tag_id` int
(11) NOT NULL,
  PRIMARY KEY
(`post_id`,`tag_id`),
  KEY `Posts_Tags_Tags_FK`
(`tag_id`),
  CONSTRAINT `posts_tags_posts_FK` FOREIGN KEY
(`post_id`) REFERENCES `posts`
(`id`),
  CONSTRAINT `posts_tags_tags_FK` FOREIGN KEY
(`tag_id`) REFERENCES `tags`
(`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tags`
--

DROP TABLE IF EXISTS `tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tags`
(
  `id` int
(11) NOT NULL AUTO_INCREMENT,
  `name` varchar
(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY
(`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_likes`
--

DROP TABLE IF EXISTS `user_likes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_likes`
(
  `user_id` int
(11) NOT NULL,
  `post_id` int
(11) NOT NULL,
  `like` tinyint
(1) DEFAULT NULL,
  PRIMARY KEY
(`user_id`,`post_id`),
  UNIQUE KEY `user_likes_UN`
(`user_id`,`post_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users`
(
  `id` int
(11) NOT NULL AUTO_INCREMENT,
  `role` enum
('Human','SuperHuman','God') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Human',
  `first_name` varchar
(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar
(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar
(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar
(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar
(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `job_desc` varchar
(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar
(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile` varchar
(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `registration_date` datetime NOT NULL,
  `profile_image_url` varchar
(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `confirmed` tinyint
(1) NOT NULL,
  PRIMARY KEY
(`id`),
  UNIQUE KEY `users_UN`
(`email`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `users_favourites`
--

DROP TABLE IF EXISTS `users_favourites`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users_favourites`
(
  `user_id` int
(11) NOT NULL,
  `post_id` int
(11) NOT NULL,
  PRIMARY KEY
(`user_id`,`post_id`),
  KEY `Users_Favourites_posts_FK`
(`post_id`),
  CONSTRAINT `users_favourites_posts_FK` FOREIGN KEY
(`post_id`) REFERENCES `posts`
(`id`),
  CONSTRAINT `users_favourites_users_FK` FOREIGN KEY
(`user_id`) REFERENCES `users`
(`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-05-13 10:33:46
