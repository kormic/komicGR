const path = require('path');
const { merge } = require('webpack-merge');
const commonConfig = require('./webpack.common');
const Dotenv = require('dotenv-webpack');
const NodemonPlugin = require('nodemon-webpack-plugin');

const devConfig = {
  stats: { errorDetails: true },
  mode: 'development',
  watchOptions: {
    ignored: /node_modules/
  },
  plugins: [
    new Dotenv({
      path: path.resolve(__dirname, '..', '.env')
    }),
    new NodemonPlugin()
  ]
};

module.exports = merge(commonConfig, devConfig);
