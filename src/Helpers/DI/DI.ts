import { Container } from 'inversify';
import * as jwt from 'jsonwebtoken';
import { multerFactory, storageOptions } from '../Multer/Config';
import * as mysql from 'promise-mysql';

import TYPES from './Types';

import ICategoryService from '@services/Category/ICategoryService';
import CategoryService from '@services/Category/CategoryService';
import PostService from '@services/Post/PostService';
import IPostService from '@services/Post/IPostService';
import IUserService from '@services/User/IUserService';
import UserService from '@services/User/UserService';
import ITagService from '@services/Tag/ITagService';
import TagService from '@services/Tag/TagService';
import { AuthorizationService } from '@services/Authorization/AuthorizationService';
import IAuthorizationService from '@services/Authorization/IAuthorizationService';
import NodeMailerWrapper from '@helpers/NodeMailer/NodeMailerWrapper';
import INodeMailerWrapper from '@helpers/NodeMailer/INodeMailerWrapper';
import BcryptService from '@helpers/Bcryptjs/BcryptService';
import IUnitOfWork from '../../UnitOfWork/IUnitOfWork';
import UnitOfWork from '../../UnitOfWork/UnitOfWork';
import ICategoryRepository from '@repos/Category/ICategoryRepository';
import CategoryRepository from '@repos/Category/CategoryRepository';
import PostRepository from '@repos/Post/PostRepository';
import IPostRepository from '@repos/Post/IPostRepository';
import IUserRepository from '@repos/User/IUserRepository';
import UserRepository from '@repos/User/UserRepository';
import ITagRepository from '@repos/Tag/ITagRepository';
import TagRepository from '@repos/Tag/TagRepository';
import { promisifyMulter } from '@helpers/Multer/PromisifyMulter';
import { getMailOptions } from '../mailOptions';
import UnitOfWorkFactory from '../../UnitOfWork/UnitOfWorkFactory';

// pool.on('acquire', function (connection) {
//     console.log('Connection %d acquired', connection.threadId);
// });

// pool.on('connection', function (connection) {
//     connection.query('SET SESSION auto_increment_increment=1')
// });

// pool.on('release', (connection) => {
//     console.log('Connection %d released', connection.threadId);
// });

export function CreateContainer(): Container {
  const container = new Container();

  container
    .bind<Promise<mysql.Pool>>(TYPES.Pool)
    .toProvider(() => async () => await UnitOfWorkFactory.createPool());
  container.bind<IUnitOfWork>(TYPES.IUnitOfWork).to(UnitOfWork);
  container.bind<ICategoryService>(TYPES.ICategoryService).to(CategoryService);
  container
    .bind<ICategoryRepository>(TYPES.ICategoryRepository)
    .to(CategoryRepository);
  container.bind<IPostService>(TYPES.IPostService).to(PostService);
  container.bind<IPostRepository>(TYPES.IPostRepository).to(PostRepository);
  container.bind<IUserService>(TYPES.IUserService).to(UserService);
  container
    .bind<IAuthorizationService>(TYPES.IAuthorizationService)
    .to(AuthorizationService);
  container.bind<IUserRepository>(TYPES.IUserRepository).to(UserRepository);
  container.bind<ITagService>(TYPES.ITagService).to(TagService);
  container.bind<ITagRepository>(TYPES.ITagRepository).to(TagRepository);
  container
    .bind<INodeMailerWrapper>(TYPES.INodeMailerWrapper)
    .toConstantValue(new NodeMailerWrapper(getMailOptions()));
  // TODO: Fix this types
  container.bind<any>(TYPES.JWT).toConstantValue(jwt);
  container.bind<any>(TYPES.MulterFactory).toConstantValue(multerFactory);
  container.bind<any>(TYPES.StorageOptions).toConstantValue(storageOptions);
  container.bind<any>(TYPES.PromisifyMulter).toConstantValue(promisifyMulter);
  container
    .bind<BcryptService>(TYPES.BcryptService)
    .toConstantValue(BcryptService);

  return container;
}
