import { injectable } from 'inversify';

import * as nodemailer from 'nodemailer';
import INodeMailerWrapper from './INodeMailerWrapper';
import MailOptions from '@dtos/MailOptionsDTO';

@injectable()
class NodeMailerWrapper implements INodeMailerWrapper {
  constructor(private transportOptions: any) {}

  private transporter = nodemailer.createTransport(this.transportOptions);

  getTransporter(): any {
    return this.transporter;
  }

  sendEmail(mailOptions: MailOptions) {
    this.transporter.sendMail(mailOptions, (error, info) => {
      if (error) {
        console.log(error);
      }
      console.log('Message sent: %s', mailOptions.to);
    });
  }
}

export default NodeMailerWrapper;
