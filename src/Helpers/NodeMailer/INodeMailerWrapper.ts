import MailOptions from '@dtos/MailOptionsDTO';

interface INodeMailerWrapper {
  getTransporter(): any;
  sendEmail(mailOptions: MailOptions);
}

export default INodeMailerWrapper;
