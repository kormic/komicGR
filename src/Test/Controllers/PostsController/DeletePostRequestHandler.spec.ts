import { Response } from 'express';
import * as sinon from 'sinon';
import * as chai from 'chai';
const expect = chai.expect;

import { handleDeletePostRequest } from '../../../WebApi/Controllers/PostsController/index';
import IPostService from '../../../Application.Services/Post/IPostService';
import { PostServiceMOCK } from '../../Mocks/PostServiceMock';
import { AuthenticatedRequest } from '../../../Domain/Models/User/AuthenticatedRequest';

describe('DeletePostRequestHandler', () => {
  let request: Partial<AuthenticatedRequest>;
  const response: Partial<Response> = {};
  let postService: IPostService;

  beforeEach(() => {
    request = {
      params: {},
      user: {}
    } as AuthenticatedRequest;
    postService = new PostServiceMOCK();
  });

  it('should return an object with success property set to true msg property set to "Post deleted" and postId property set to the deleted post id', async () => {
    const userId = 1;
    const postId = 5;
    const stubDeletePost = sinon
      .stub(postService, 'deletePostById')
      .callsFake(() => Promise.resolve(true));
    const stubGetPostById = sinon
      .stub(postService, 'getPostByPostId')
      .callsFake(() => Promise.resolve({ user_id: userId }));
    (request as AuthenticatedRequest).user.id = userId;
    (request as AuthenticatedRequest).params.postId = String(postId);
    const handler = handleDeletePostRequest(
      request as AuthenticatedRequest,
      response as Response,
      postService
    );
    const result = await handler;

    stubDeletePost.restore();
    stubGetPostById.restore();
    expect(handler).to.be.instanceOf(Promise);
    expect(result.postId).equal(postId);
    expect(result.success).equal(true);
    expect(result.msg).equal('Post deleted');
  });

  it('should return an object with success property set to false and msg property set to "Post does not exist" when the post does not exist', async () => {
    const postId = 5;
    const spyDeletePost = sinon.spy(postService, 'deletePostById');
    const stubGetPostById = sinon
      .stub(postService, 'getPostByPostId')
      .callsFake(() => Promise.resolve(undefined));
    (request as AuthenticatedRequest).params.id = String(postId);
    const handler = handleDeletePostRequest(
      request as AuthenticatedRequest,
      response as Response,
      postService
    );
    const result = await handler;

    spyDeletePost.restore();
    stubGetPostById.restore();
    expect(handler).to.be.instanceOf(Promise);
    expect(result.success).equal(false);
    expect(result.msg).equal('Post does not exist');
    expect(result.postId).to.be.undefined;
    expect(spyDeletePost.called).to.be.false;
  });

  it('should return an object with success property set to false and msg property set to "You are not allowed to delete this post" when the post does not belong to this user', async () => {
    const userId = 1;
    const postId = '5';
    const userPostId = 7;
    const stubDeletePost = sinon
      .stub(postService, 'deletePostById')
      .callsFake(() => Promise.resolve(false));
    const stubGetPostById = sinon
      .stub(postService, 'getPostByPostId')
      .callsFake(() => Promise.resolve({ user_id: userPostId }));

    (request as AuthenticatedRequest).user.id = userId;
    (request as AuthenticatedRequest).params.id = postId;
    const handler = handleDeletePostRequest(
      request as AuthenticatedRequest,
      response as Response,
      postService
    );
    const result = await handler;

    stubDeletePost.restore();
    stubGetPostById.restore();
    expect(handler).to.be.instanceOf(Promise);
    expect(result.success).equal(false);
    expect(result.msg).equal('You are not allowed to delete this post');
    expect(result.postId).to.be.undefined;
  });

  it('should return an object with success property set to false and msg property set to "Something went wrong"', async () => {
    const userId = 1;
    const postId = '5';
    const stubDeletePost = sinon
      .stub(postService, 'deletePostById')
      .callsFake(() => Promise.resolve(false));
    const stubGetPostById = sinon
      .stub(postService, 'getPostByPostId')
      .callsFake(() => Promise.resolve({ user_id: userId }));
    (request as AuthenticatedRequest).user.id = userId;
    (request as AuthenticatedRequest).params.postId = postId;
    const handler = handleDeletePostRequest(
      request as AuthenticatedRequest,
      response as Response,
      postService
    );
    const result = await handler;

    stubDeletePost.restore();
    stubGetPostById.restore();
    expect(handler).to.be.instanceOf(Promise);
    expect(result.success).equal(false);
    expect(result.postId).to.be.undefined;
    expect(result.msg).equal('Something went wrong');
  });
});
