import { Response } from 'express';
import sinon from 'sinon';
import chai from 'chai';
const expect = chai.expect;

import { handleEditPost } from '../../../WebApi/Controllers/PostsController/index';
import IPostService from '../../../Application.Services/Post/IPostService';
import { PostServiceMOCK } from '../../Mocks/PostServiceMock';
import EditPostDTO from '../../../Domain/Dtos/Post/EditPostDTO';
import User from '../../../Domain/Models/User/User';
import { AuthenticatedRequest } from '../../../Domain/Models/User/AuthenticatedRequest';

describe('EditPostRequestHandler', () => {
  let request: Partial<AuthenticatedRequest>;
  const response: Partial<Response> = {};
  let postService: IPostService;

  beforeEach(() => {
    request = {
      user: { id: 15 } as User,
      params: {}
    };
    postService = new PostServiceMOCK();
  });
  it('should return an object with id property and value set to the edited post id, if the user owns the post', async () => {
    const editPosttDTO: EditPostDTO = {
      title: 'test title',
      short_body: 'test short_body',
      body: 'test body',
      imageUrl: 'path_to_image_url',
      categoryId: 1
    };
    request.body = editPosttDTO;
    request.params!.postId = '1';
    const stubEditPost = sinon
      .stub(postService, 'editPost')
      .callsFake(() => Promise.resolve({ id: 1 }));

    const handler = handleEditPost(
      request as AuthenticatedRequest,
      response as Response,
      postService
    );
    const id = await handler;

    expect(id).equal(+request.params!.postId);
    stubEditPost.restore();
  });
  it('should return an object with id property set to the edited post id, when post is updated successfuly', async () => {
    const editPosttDTO: EditPostDTO = {
      title: 'test title',
      short_body: 'test short_body',
      body: 'test body',
      imageUrl: 'path_to_image_url',
      categoryId: 1
    };
    request.params!.postId = '1';
    request.body = editPosttDTO;
    const stubEditPost = sinon.spy(postService, 'editPost');
    const handler = handleEditPost(
      request as AuthenticatedRequest,
      response as Response,
      postService
    );
    const id = await handler;

    stubEditPost.restore();
    expect(stubEditPost.calledOnceWith(+request.params!.postId, editPosttDTO))
      .to.be.true;
    expect(handler).to.be.instanceOf(Promise);
    expect(id).equal(+request.params!.postId);
  });
  it('should throw an error with message "Failed to update post", if the post fails to be updated', async () => {
    const editPosttDTO: EditPostDTO = {
      title: 'test title',
      short_body: 'test short_body',
      body: 'test body',
      imageUrl: 'path_to_image_url',
      categoryId: 1
    };
    request.params!.postId = '1';
    request.body = editPosttDTO;
    const stubEditPost = sinon
      .stub(postService, 'editPost')
      .callsFake(() => Promise.reject(new Error('Failed to update post')));
    const handler = handleEditPost(
      request as AuthenticatedRequest,
      response as Response,
      postService
    );

    try {
      await handler;

      throw new Error(
        "Promise should reject with message 'Failed to update post'"
      );
    } catch (error) {
      expect(stubEditPost.calledOnceWith(+request.params!.postId, editPosttDTO))
        .to.be.true;
      expect(error.message).to.equal('Failed to update post');
    }
    stubEditPost.restore();
  });
  it('should return an object with property errorMessage and value "Required fields are missing", if fields are missing', async () => {
    const editPosttDTO: EditPostDTO = {
      short_body: 'test short_body',
      body: 'test body',
      imageUrl: 'path_to_image_url',
      categoryId: 1
    } as EditPostDTO;
    request.body = editPosttDTO;
    const stubEditPost = sinon.stub(postService, 'editPost');
    const handler = handleEditPost(
      request as AuthenticatedRequest,
      response as Response,
      postService
    );

    try {
      await handler;

      throw new Error(
        "Promise should reject with message 'Required fields are missing'"
      );
    } catch (error) {
      expect(stubEditPost.called).to.be.false;
      expect(error.message).to.equal('Required fields are missing');
    }
    stubEditPost.restore();
  });
  it(`should return an object with property errorMessage and value "Post doesn't exist'", if post doesn't exist`, async () => {
    request.params!.postID = '-1';
    const stubGetPostById = sinon
      .stub(postService, 'getPostByPostId')
      .callsFake(() => Promise.reject(new Error("Post doesn't exist")));
    const handler = handleEditPost(
      request as AuthenticatedRequest,
      response as Response,
      postService
    );

    try {
      await handler;

      throw new Error(
        "Promise should reject with message 'Post doesn't exist'"
      );
    } catch (error) {
      expect(stubGetPostById.calledOnceWith(+request.params!.postId)).to.be
        .true;
      expect(error.message).to.equal("Post doesn't exist");
    }
    stubGetPostById.restore();
  });
  it('should return an object with property errorMessage and value "You are not authorized to edit this post", if the user doesn\'t own the post', async () => {
    request.params!.postID = '1';
    const stubGetPostById = sinon
      .stub(postService, 'getPostByPostId')
      .callsFake(() =>
        Promise.reject(new Error('You are not authorized to edit this post'))
      );
    const handler = handleEditPost(
      request as AuthenticatedRequest,
      response as Response,
      postService
    );

    try {
      await handler;

      throw new Error(
        "Promise should reject with message 'You are not authorized to edit this post'"
      );
    } catch (error) {
      expect(stubGetPostById.calledOnceWith(+request.params!.postId)).to.be
        .true;
      expect(error.message).to.equal(
        'You are not authorized to edit this post'
      );
    }
    stubGetPostById.restore();
  });
});
