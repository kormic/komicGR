import { Response } from 'express';
import sinon from 'sinon';
import chai from 'chai';
const expect = chai.expect;

import { handleSetPostAsUserFavourite } from '../../../WebApi/Controllers/PostsController/index';
import IPostService from '../../../Application.Services/Post/IPostService';
import { PostServiceMOCK } from '../../Mocks/PostServiceMock';
import PostDTO from '../../../Domain/Dtos/Post/PostDTO';
import User from '../../../Domain/Models/User/User';
import { AuthenticatedRequest } from '../../../Domain/Models/User/AuthenticatedRequest';

describe('SetPostAsUserFavouriteHandler', () => {
  let request: Partial<AuthenticatedRequest>;
  let response: Response;
  let postService: IPostService;

  beforeEach(() => {
    request = {
      body: {},
      user: {} as User
    };
    postService = new PostServiceMOCK();
  });

  it('should return the postId on success', async () => {
    const userId = 1;
    const postId = 100;
    const stubLikePost = sinon
      .stub(postService, 'setPostAsUserFavourite')
      .callsFake((postId: number, userId: number) =>
        Promise.resolve({ postId: 100 })
      );
    const handler = handleSetPostAsUserFavourite(postId, userId, postService);
    const result = await handler;

    stubLikePost.restore();
    expect(handler).to.be.instanceOf(Promise);
    expect(stubLikePost.calledOnceWith(postId, userId));
    expect(result).deep.equal({ postId: 100 });
  });
});
