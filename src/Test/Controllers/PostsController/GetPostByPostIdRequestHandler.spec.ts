import { Response } from 'express';
import sinon from 'sinon';
import chai from 'chai';
const expect = chai.expect;

import { handleGetPostByPostId } from '../../../WebApi/Controllers/PostsController/index';
import IPostService from '../../../Application.Services/Post/IPostService';
import { PostServiceMOCK } from '../../Mocks/PostServiceMock';
import Post from '../../../Domain/Models/Post/Post';
import { createTestPost } from '../../Helpers/postUtils';
import { AuthenticatedRequest } from '../../../Domain/Models/User/AuthenticatedRequest';

describe('GetPostByPostIdRequestHandler', () => {
  let request: Partial<AuthenticatedRequest> = {};
  const response: Partial<Response> = {};
  let postService: IPostService;

  beforeEach(() => {
    request = {
      params: {}
    };
    postService = new PostServiceMOCK();
  });

  it('should return an object with a post property and as value the post response', async () => {
    const dummyPost = createTestPost();
    const stubGetPostByPostId = sinon
      .stub(postService, 'getPostByPostId')
      .callsFake((postId: number) => Promise.resolve(dummyPost));
    request.params!.postId = String(dummyPost.id);
    const handler = handleGetPostByPostId(
      request as AuthenticatedRequest,
      response as Response,
      postService
    );
    const result = await handler;

    stubGetPostByPostId.restore();
    expect(stubGetPostByPostId.calledOnceWith(dummyPost.id)).to.be.true;
    expect(result.post).deep.equal(Post.toPostResponse(dummyPost));
  });
});
