import { stub, spy } from 'sinon';
import chai from 'chai';
const { expect } = chai;

import { handleAddPost } from '../../../WebApi/Controllers/PostsController/index';
import IPostService from '../../../Application.Services/Post/IPostService';
import { PostServiceMOCK } from '../../Mocks/PostServiceMock';
import AddPostDTO from '../../../Domain/Dtos/Post/AddPostDTO';
import { AuthorizationServiceMOCK } from '../../Mocks/AuthorizationService';
import { AuthenticatedRequest } from '../../../Domain/Models/User/AuthenticatedRequest';

describe('AddPostRequestHandler', () => {
  const request: Partial<AuthenticatedRequest> = {};
  const authorizationService = new AuthorizationServiceMOCK();
  let postService: IPostService;

  beforeEach(() => {
    postService = new PostServiceMOCK();
  });

  it('should return success false and the msg "You are not allowed to add a post"', async () => {
    request.body = {
      user_id: 1,
      title: 'test title',
      short_body: 'test short_body',
      body: 'test body',
      createdAt: '2019/06/01',
      imageUrl: 'path_to_image_url',
      categoryId: 1
    };
    const stubAddPost = stub(postService, 'addPost');
    const stubGetCanAddPost = stub(
      authorizationService,
      'getCanAddPost'
    ).callsFake(() => false);
    const handler = handleAddPost(
      request as AuthenticatedRequest,
      postService,
      authorizationService
    );

    try {
      await handler;
      expect.fail();
    } catch (error) {
      expect(stubAddPost.called).to.be.false;
      expect(error).to.deep.equal({
        stack: null,
        message: 'You are not allowed to add a post'
      });
    }

    stubAddPost.restore();
    stubGetCanAddPost.restore();
  });

  it('should return an object with success property set to true msg property set to "Post added" and postId property set to the inserted post id', async () => {
    const addPostDTO: AddPostDTO = {
      user_id: 1,
      title: 'test title',
      short_body: 'test short_body',
      body: 'test body',
      createdAt: '2019/06/01',
      imageUrl: 'path_to_image_url',
      categoryId: 1
    };
    request.body = addPostDTO;
    const stubAddPost = stub(postService, 'addPost').callsFake(() =>
      Promise.resolve({ insertId: 1 })
    );
    const stubGetCanAddPost = stub(
      authorizationService,
      'getCanAddPost'
    ).callsFake(() => true);

    const handler = handleAddPost(
      request as AuthenticatedRequest,
      postService,
      authorizationService
    );
    const result = await handler;

    stubAddPost.restore();
    stubGetCanAddPost.restore();
    expect(handler).to.be.instanceOf(Promise);
    expect(result.postID).equal(1);
    expect(result.success).equal(true);
    expect(result.msg).equal('Post added');
  });

  it('should return an object with success property set to false and msg property set to "Required fields are missing" when user_id is missing from the dto', async () => {
    const addPostDTO = {
      title: 'test title',
      short_body: 'test short_body',
      body: 'test body',
      createdAt: '2019/06/01',
      imageUrl: 'path_to_image_url',
      categoryId: 1
    } as AddPostDTO;
    request.body = addPostDTO;
    const spyAddPost = spy(postService, 'addPost');
    const stubGetCanAddPost = stub(
      authorizationService,
      'getCanAddPost'
    ).callsFake(() => true);
    const handler = handleAddPost(
      request as AuthenticatedRequest,
      postService,
      authorizationService
    );
    const result = await handler;

    spyAddPost.restore();
    stubGetCanAddPost.restore();
    expect(spyAddPost.calledOnce).to.be.false;
    expect(handler).to.be.instanceOf(Promise);
    expect(result).to.deep.equal({
      success: false,
      msg: 'Required fields are missing'
    });
  });

  it('should return an object with success property set to false and msg property set to "Required fields are missing" when title is missing from the dto', async () => {
    const addPostDTO = {
      user_id: 1,
      short_body: 'test short_body',
      body: 'test body',
      createdAt: '2019/06/01',
      imageUrl: 'path_to_image_url',
      categoryId: 1
    } as AddPostDTO;
    request.body = addPostDTO;
    const spyAddPost = spy(postService, 'addPost');
    const stubGetCanAddPost = stub(
      authorizationService,
      'getCanAddPost'
    ).callsFake(() => true);
    const handler = handleAddPost(
      request as AuthenticatedRequest,
      postService,
      authorizationService
    );
    const result = await handler;

    spyAddPost.restore();
    stubGetCanAddPost.restore();
    expect(spyAddPost.calledOnce).to.be.false;
    expect(handler).to.be.instanceOf(Promise);
    expect(result).to.deep.equal({
      success: false,
      msg: 'Required fields are missing'
    });
  });

  it('should return an object with success property set to false and msg property set to "Required fields are missing" when short_body is missing from the dto', async () => {
    const addPostDTO = {
      user_id: 1,
      title: 'test title',
      body: 'test body',
      createdAt: '2019/06/01',
      imageUrl: 'path_to_image_url',
      categoryId: 1
    } as AddPostDTO;
    request.body = addPostDTO;
    const spyAddPost = spy(postService, 'addPost');
    const stubGetCanAddPost = stub(
      authorizationService,
      'getCanAddPost'
    ).callsFake(() => true);
    const handler = handleAddPost(
      request as AuthenticatedRequest,
      postService,
      authorizationService
    );
    const result = await handler;

    spyAddPost.restore();
    stubGetCanAddPost.restore();
    expect(spyAddPost.calledOnce).to.be.false;
    expect(handler).to.be.instanceOf(Promise);
    expect(result).to.deep.equal({
      success: false,
      msg: 'Required fields are missing'
    });
  });

  it('should return an object with success property set to false and msg property set to "Required fields are missing" when body is missing from the dto', async () => {
    const addPostDTO = {
      user_id: 1,
      title: 'test title',
      short_body: 'test short_body',
      createdAt: '2019/06/01',
      imageUrl: 'path_to_image_url',
      categoryId: 1
    } as AddPostDTO;
    request.body = addPostDTO;
    const spyAddPost = spy(postService, 'addPost');
    const stubGetCanAddPost = stub(
      authorizationService,
      'getCanAddPost'
    ).callsFake(() => true);
    const handler = handleAddPost(
      request as AuthenticatedRequest,
      postService,
      authorizationService
    );
    const result = await handler;

    spyAddPost.restore();
    stubGetCanAddPost.restore();
    expect(spyAddPost.calledOnce).to.be.false;
    expect(handler).to.be.instanceOf(Promise);
    expect(result).to.deep.equal({
      success: false,
      msg: 'Required fields are missing'
    });
  });

  it('should return an object with success property set to false and msg property set to "Required fields are missing" when categoryId is missing from the dto', async () => {
    const addPostDTO = {
      user_id: 1,
      title: 'test title',
      short_body: 'test short_body',
      body: 'test body',
      createdAt: '2019/06/01',
      imageUrl: 'path_to_image_url'
    } as AddPostDTO;
    request.body = addPostDTO;
    const spyAddPost = spy(postService, 'addPost');
    const stubGetCanAddPost = stub(
      authorizationService,
      'getCanAddPost'
    ).callsFake(() => true);
    const handler = handleAddPost(
      request as AuthenticatedRequest,
      postService,
      authorizationService
    );
    const result = await handler;

    spyAddPost.restore();
    stubGetCanAddPost.restore();
    expect(spyAddPost.calledOnce).to.be.false;
    expect(handler).to.be.instanceOf(Promise);
    expect(result).to.deep.equal({
      success: false,
      msg: 'Required fields are missing'
    });
  });

  it('should return an object with success property set to false and msg property set to "Title is too short. It should be at least 3 characters" when title is less than 3 characters', async () => {
    const addPostDTO = {
      user_id: 1,
      title: 'te',
      short_body: 'test short_body',
      body: 'test body',
      createdAt: '2019/06/01',
      imageUrl: 'path_to_image_url',
      categoryId: 1
    } as AddPostDTO;
    request.body = addPostDTO;
    const spyAddPost = spy(postService, 'addPost');
    const stubGetCanAddPost = stub(
      authorizationService,
      'getCanAddPost'
    ).callsFake(() => true);
    const handler = handleAddPost(
      request as AuthenticatedRequest,
      postService,
      authorizationService
    );
    const result = await handler;

    spyAddPost.restore();
    stubGetCanAddPost.restore();
    expect(spyAddPost.calledOnce).to.be.false;
    expect(handler).to.be.instanceOf(Promise);
    expect(result).to.deep.equal({
      success: false,
      msg: 'Title is too short. It should be at least 3 characters'
    });
  });
});
