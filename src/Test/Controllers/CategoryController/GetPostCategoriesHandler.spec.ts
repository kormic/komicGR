import { Response } from 'express';
import sinon from 'sinon';
import chai from 'chai';
const expect = chai.expect;

import { handleGetPostCategories } from '../../../WebApi/Controllers/CategoryController/index';

import ICategoryService from '../../../Application.Services/Category/ICategoryService';
import { CategoryServiceMOCK } from '../../Mocks/CategoryServiceMock';
import Category from '../../../Domain/Models/Category/Category';
import { AuthenticatedRequest } from '../../../Domain/Models/User/AuthenticatedRequest';

describe('GetPostsCategoriesHandler', () => {
  let request: Partial<AuthenticatedRequest>;
  let response: Response;
  let categoryService: ICategoryService;

  beforeEach(() => {
    request = {
      body: {
        username: 'username',
        password: 'password'
      },
      params: {}
    };
    categoryService = new CategoryServiceMOCK();
  });

  it('should return an object with success property set to true and categories property with the categories', async () => {
    const dummyCategories = [
      new Category(1, 'test category 1', 'desc 1'),
      new Category(2, 'test category 2', 'desc 2')
    ];
    const stubGetPostCategories = sinon
      .stub(categoryService, 'getPostCategories')
      .callsFake(() => Promise.resolve(dummyCategories));
    const handler = handleGetPostCategories(
      request as AuthenticatedRequest,
      response,
      categoryService
    );
    const categoriesResponse = await handler;

    stubGetPostCategories.restore();
    expect(handler).to.be.instanceOf(Promise);
    expect(categoriesResponse).to.have.property('success').and.equal(true);
    expect(categoriesResponse)
      .to.have.property('categories')
      .and.deep.equal(dummyCategories);
  });
});
