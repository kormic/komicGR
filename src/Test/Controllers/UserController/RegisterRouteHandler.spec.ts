import { Response } from 'express';
import sinon from 'sinon';
import chai from 'chai';
const expect = chai.expect;

import { handleRegisterRequest } from '../../../WebApi/Controllers/UserController/index';

import IUserService from '../../../Application.Services/User/IUserService';
import { UserServiceMOCK } from '../../Mocks/UserServiceMock';
import INodeMailerWrapper from '../../../Helpers/NodeMailer/INodeMailerWrapper';

import RegisterUserDTO from '../../../Domain/Dtos/User/RegisterUserDTO';
import User from '../../../Domain/Models/User/User';
import Role from '../../../Domain/Models/Role/Role';
import { getConfirmationMailOptions } from '../../../WebApi/Controllers/UserController/RegisterRouteHandler';
import { AuthenticatedRequest } from '../../../Domain/Models/User/AuthenticatedRequest';

function createTestUser(): User {
  return new User(
    1,
    Role.Human,
    'first_name',
    'last_name',
    'test@test.com',
    'test username',
    'test pass',
    'test job_desc',
    'test address',
    111111111,
    'http://www.someurl.com',
    new Date()
  );
}

function createRegisterUserDTO(): RegisterUserDTO {
  return {
    first_name: 'first_name',
    last_name: 'last_name',
    email: 'test@test.com',
    username: 'test username',
    password: 'test pass',
    job_desc: 'test job_desc',
    address: 'test address',
    mobile: 111111111
  };
}

function createRegisterUserDTOWithInvalidUsername(): RegisterUserDTO {
  return {
    first_name: 'first_name',
    last_name: 'last_name',
    email: 'test@test.com',
    username: 'te',
    password: 'test pass',
    job_desc: 'test job_desc',
    address: 'test address',
    mobile: 111111111
  };
}

function createRegisterUserDTOWithInvalidPassword(): RegisterUserDTO {
  return {
    first_name: 'first',
    last_name: 'last_name',
    email: 'test@test.com',
    username: 'test username',
    password: 'pass',
    job_desc: 'test job_desc',
    address: 'test address',
    mobile: 111111111
  };
}

class NodeMailerWrapperDouble implements INodeMailerWrapper {
  getTransporter() {
    return null;
  }
  sendEmail() {
    return 'email';
  }
}

describe('RegisterRouteRequestHandler', () => {
  const request: Partial<AuthenticatedRequest> = {};
  let response: Response;
  let userService: IUserService;
  let nodeMailerWrapperDouble: INodeMailerWrapper;
  const token = 'token';
  const jwt = {
    sign: () => token
  };

  beforeEach(() => {
    userService = new UserServiceMOCK();
    nodeMailerWrapperDouble = new NodeMailerWrapperDouble();
  });

  describe('success cases', () => {
    it('should return an object with success property set to true and msg property set to "User registered"', async () => {
      const newUser = createRegisterUserDTO();
      request.body = newUser;
      const testUser = createTestUser();
      const stubGetByUsername = sinon
        .stub(userService, 'getByUsername')
        .callsFake(() => Promise.resolve(null));
      const stubGetByEmail = sinon
        .stub(userService, 'getByEmail')
        .callsFake(() => Promise.resolve(null));
      const stubInsertUser = sinon
        .stub(userService, 'insertUser')
        .callsFake(() => Promise.resolve({ insertId: testUser.id }));

      const stubSendEmail = sinon
        .stub(nodeMailerWrapperDouble, 'sendEmail')
        .callsFake(() => ({}));

      const handler = handleRegisterRequest(
        request as AuthenticatedRequest,
        response,
        userService,
        nodeMailerWrapperDouble,
        jwt
      );
      const registerResponse = await handler;
      stubGetByUsername.restore();
      stubGetByEmail.restore();
      stubInsertUser.restore();
      stubSendEmail.restore();

      expect(handler).to.be.instanceOf(Promise);
      expect(registerResponse).to.deep.equal({
        success: true,
        msg: 'User registered',
        userId: testUser.id
      });
      expect(stubGetByUsername.calledOnceWith(request.body.username)).to.be
        .true;
      expect(stubGetByEmail.calledOnceWith(request.body.email)).to.be.true;
      expect(stubInsertUser.calledOnceWith(request.body)).to.be.true;
      expect(
        stubSendEmail.calledOnceWith(getConfirmationMailOptions(newUser, token))
      ).to.be.true;
    });
  });

  describe('error cases', () => {
    it('should return an object with success property set to false and msg property set to "Username must have at least 3 characters."', async () => {
      const spyGetByUsername = sinon.spy(userService, 'getByUsername');
      const spyGetByEmail = sinon.spy(userService, 'getByEmail');
      const spyInsertUser = sinon.spy(userService, 'insertUser');
      const spySendEmail = sinon.spy(nodeMailerWrapperDouble, 'sendEmail');
      request.body = createRegisterUserDTOWithInvalidUsername();

      const handler = handleRegisterRequest(
        request as AuthenticatedRequest,
        response,
        userService,
        nodeMailerWrapperDouble,
        jwt
      );
      const registerResponse = await handler;
      spyGetByUsername.restore();
      spyGetByEmail.restore();
      spyInsertUser.restore();
      spySendEmail.restore();

      expect(handler).to.be.instanceOf(Promise);
      expect(registerResponse).to.deep.equal({
        success: false,
        msg: 'Username must have at least 3 characters.'
      });
      expect(spyGetByUsername.calledOnceWith(request.body.username)).to.be
        .false;
      expect(spyGetByEmail.calledOnceWith(request.body.email)).to.be.false;
      expect(spyInsertUser.calledOnceWith(request.body)).to.be.false;
      expect(spySendEmail.calledOnce).to.be.false;
    });
    it('should return an object with success property set to false and msg property set to "Password must have at least 8 characters."', async () => {
      const spyGetByUsername = sinon.spy(userService, 'getByUsername');
      const spyGetByEmail = sinon.spy(userService, 'getByEmail');
      const spyInsertUser = sinon.spy(userService, 'insertUser');
      const spySendEmail = sinon.spy(nodeMailerWrapperDouble, 'sendEmail');
      request.body = createRegisterUserDTOWithInvalidPassword();

      const handler = handleRegisterRequest(
        request as AuthenticatedRequest,
        response,
        userService,
        nodeMailerWrapperDouble,
        jwt
      );
      const registerResponse = await handler;
      spyGetByUsername.restore();
      spyGetByEmail.restore();
      spyInsertUser.restore();
      spySendEmail.restore();

      expect(handler).to.be.instanceOf(Promise);
      expect(registerResponse).to.deep.equal({
        success: false,
        msg: 'Password must have at least 8 characters.'
      });
      expect(spyGetByUsername.calledOnceWith(request.body.username)).to.be
        .false;
      expect(spyGetByEmail.calledOnceWith(request.body.email)).to.be.false;
      expect(spyInsertUser.calledOnceWith(request.body)).to.be.false;
      expect(spySendEmail.calledOnce).to.be.false;
    });
    it('should throw an error with a message "This username already exists"', async () => {
      request.body = createRegisterUserDTO();
      const testUser = createTestUser();
      const stubGetByUsername = sinon
        .stub(userService, 'getByUsername')
        .callsFake(() => Promise.resolve(testUser));
      const spyGetByEmail = sinon.spy(userService, 'getByEmail');
      const spyInsertUser = sinon.spy(userService, 'insertUser');
      const spySendEmail = sinon.spy(nodeMailerWrapperDouble, 'sendEmail');

      try {
        await handleRegisterRequest(
          request as AuthenticatedRequest,
          response,
          userService,
          nodeMailerWrapperDouble,
          jwt
        );
        expect.fail('Expected to throw Error (This username already exists)');
      } catch (error) {
        expect(error).to.deep.equal({
          message: 'This username already exists'
        });
        expect(stubGetByUsername.calledOnceWith(request.body.username)).to.be
          .true;
        expect(spyGetByEmail.calledOnceWith(request.body.email)).to.be.false;
        expect(spyInsertUser.calledOnceWith(request.body)).to.be.false;
        expect(spySendEmail.calledOnce).to.be.false;
      }

      stubGetByUsername.restore();
      spyGetByEmail.restore();
      spyInsertUser.restore();
      spySendEmail.restore();
    });
    it('should throw an error with message "This email already exists"', async () => {
      request.body = createRegisterUserDTO();
      const testUser = createTestUser();
      const stubGetByUsername = sinon
        .stub(userService, 'getByUsername')
        .callsFake(() => Promise.resolve(null));
      const stubGetByEmail = sinon
        .stub(userService, 'getByEmail')
        .callsFake(() => Promise.resolve(testUser));
      const spyInsertUser = sinon.spy(userService, 'insertUser');
      const spySendEmail = sinon.spy(nodeMailerWrapperDouble, 'sendEmail');

      try {
        await handleRegisterRequest(
          request as AuthenticatedRequest,
          response,
          userService,
          nodeMailerWrapperDouble,
          jwt
        );
        expect.fail('Expected to throw Error (This email already exists)');
      } catch (error) {
        expect(error).to.deep.equal({ message: 'This email already exists' });
        expect(stubGetByUsername.calledOnceWith(request.body.username)).to.be
          .true;
        expect(stubGetByEmail.calledOnceWith(request.body.email)).to.be.true;
        expect(spyInsertUser.calledOnceWith(request.body)).to.be.false;
        expect(spySendEmail.calledOnce).to.be.false;
      }

      stubGetByUsername.restore();
      stubGetByEmail.restore();
      spyInsertUser.restore();
      spySendEmail.restore();
    });
  });
});
