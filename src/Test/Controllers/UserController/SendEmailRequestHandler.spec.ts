import { Response } from 'express';
import sinon from 'sinon';
import chai from 'chai';
const expect = chai.expect;

import { handleSendEmailRequest } from '../../../WebApi/Controllers/UserController/index';
import INodeMailerWrapper from '../../../Helpers/NodeMailer/INodeMailerWrapper';
import IUserService from '../../../Application.Services/User/IUserService';
import { UserServiceMOCK } from '../../Mocks/UserServiceMock';
import User from '../../../Domain/Models/User/User';
import Role from '../../../Domain/Models/Role/Role';
import { getResetMailOptions } from '../../..//WebApi/Controllers/UserController/SendEmailRequestHandler';
import { AuthenticatedRequest } from '../../../Domain/Models/User/AuthenticatedRequest';

class NodeMailerWrapperDouble implements INodeMailerWrapper {
  getTransporter() {
    return null;
  }
  sendEmail() {
    return {};
  }
}

function createTestUser(): User {
  return new User(
    1,
    Role.Human,
    'first_name',
    'last_name',
    'test@test.com',
    'test username',
    'test pass',
    'test job_desc',
    'test address',
    111111111,
    'http://www.someurl.com',
    new Date()
  );
}

describe('SendEmailRequestHandler', () => {
  let request: Partial<AuthenticatedRequest>;
  let response: Response;
  let userServiceDouble: IUserService;
  let nodeMailerWrapperDouble: INodeMailerWrapper;
  const token = 'token';
  const jwt = {
    sign: () => token
  };

  beforeEach(() => {
    request = {
      body: {}
    };
    nodeMailerWrapperDouble = new NodeMailerWrapperDouble();
    userServiceDouble = new UserServiceMOCK();
  });

  it('should return true if the mail has been sent successfully', async () => {
    const testUser = createTestUser();
    request.body.email = testUser.email;

    const stubGetByEmail = sinon
      .stub(userServiceDouble, 'getByEmail')
      .callsFake(() => Promise.resolve(testUser));
    const stubSendEmail = sinon
      .stub(nodeMailerWrapperDouble, 'sendEmail')
      .callsFake(() => ({}));
    const spyJWT = sinon.spy(jwt, 'sign');

    const handler = handleSendEmailRequest(
      request as AuthenticatedRequest,
      response,
      userServiceDouble,
      nodeMailerWrapperDouble,
      jwt
    );
    const sendEmailResponse = await handler;

    stubGetByEmail.restore();
    stubSendEmail.restore();
    spyJWT.restore();

    expect(sendEmailResponse).to.equal(true);
    expect(stubGetByEmail.calledOnceWith(testUser.email)).to.be.true;
    expect(spyJWT.calledAfter(stubGetByEmail)).to.be.true;
    expect(spyJWT.calledBefore(stubSendEmail)).to.be.true;
    expect(spyJWT.calledOnce).to.be.true;
    expect(stubSendEmail.calledAfter(stubGetByEmail)).to.be.true;
    expect(
      stubSendEmail.calledOnceWith(getResetMailOptions(testUser.email, token))
    ).to.be.true;
  });
  it('should throw an error if the user service throws an error', async () => {
    request.body.email = 'arandom@email.com';

    const stubGetByEmail = sinon
      .stub(userServiceDouble, 'getByEmail')
      .throws({ message: 'ErrorMessage', stack: 'ErrorStack' });
    const spySendEmail = sinon.spy(nodeMailerWrapperDouble, 'sendEmail');
    const spyJWT = sinon.spy(jwt, 'sign');

    const handler = handleSendEmailRequest(
      request as AuthenticatedRequest,
      response,
      userServiceDouble,
      nodeMailerWrapperDouble,
      jwt
    );

    try {
      await handler;
    } catch (error) {
      expect(error).to.deep.equal({
        message: 'ErrorMessage',
        stack: 'ErrorStack'
      });
      stubGetByEmail.restore();
      spyJWT.restore();

      expect(stubGetByEmail.calledOnceWith('arandom@email.com')).to.be.true;
      expect(spyJWT.calledOnce).to.be.false;
      expect(spySendEmail.called).to.be.false;
    }
  });
  it('should return false if the mail does not belong to a user', async () => {
    request.body.email = 'arandom@email.com';

    const stubGetByEmail = sinon
      .stub(userServiceDouble, 'getByEmail')
      .callsFake(() => Promise.resolve(null));
    const spySendEmail = sinon.spy(nodeMailerWrapperDouble, 'sendEmail');
    const spyJWT = sinon.spy(jwt, 'sign');

    const handler = handleSendEmailRequest(
      request as AuthenticatedRequest,
      response,
      userServiceDouble,
      nodeMailerWrapperDouble,
      jwt
    );

    const sendEmailResponse = await handler;

    stubGetByEmail.restore();

    expect(sendEmailResponse).to.equal(false);
    expect(stubGetByEmail.calledOnceWith('arandom@email.com')).to.be.true;
    expect(spyJWT.calledOnce).to.be.false;
    expect(spySendEmail.called).to.be.false;
  });
});
