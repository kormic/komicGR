import { Response } from 'express';
import chai from 'chai';
const expect = chai.expect;

import { handleProfileRequest } from '../../../WebApi/Controllers/UserController/index';

import User from '../../../Domain/Models/User/User';
import Role from '../../../Domain/Models/Role/Role';
import ProfileUserResponseDTO from '../../../Domain/Dtos/User/ProfileUserResponseDTO';
import { AuthenticatedRequest } from '../../../Domain/Models/User/AuthenticatedRequest';

function createTestUserWithId(id: number): User {
  return new User(
    id,
    Role.Human,
    'Test First Name',
    'Test Last Name',
    'test' + id + '@test.com',
    'test_username_' + id,
    'test_pass',
    'test job_desc',
    'test address',
    11111111,
    'http://www.someurl.com',
    new Date()
  );
}

describe('ProfileRouteRequestHandler', () => {
  let request: Partial<AuthenticatedRequest>;
  let response: Partial<Response>;

  beforeEach(() => {
    request = {
      user: createTestUserWithId(1)
    };
  });

  it('should return an object with a user property and a value of the profile user response', () => {
    const profileResponse = handleProfileRequest(
      request as AuthenticatedRequest,
      response as Response
    );

    expect(profileResponse).to.deep.equal({
      user: <ProfileUserResponseDTO>{
        ...request.user,
        registration_date: request.user!.registration_date.toString()
      }
    });
  });
});
