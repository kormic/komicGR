import { Response } from 'express';
import { Container } from 'inversify';
import sinon from 'sinon';
import chai from 'chai';

const expect = chai.expect;

import TYPES from '../../Helpers/DI/Types';
import * as TagHandlers from '../../WebApi/Controllers/TagsController/GetTagsRequestHandler';
import { PostRoute } from '../../WebApi/Routes/v1/PostRoutes';
import * as ErrorHandler from '../../WebApi/Routes/v1/utils';
import IPostService from '../../Application.Services/Post/IPostService';
import { PostServiceMOCK } from '../Mocks/PostServiceMock';
import { TagServiceMOCK } from '../Mocks/TagServiceMock';
import Post from '../../Domain/Models/Post/Post';
import LikePostDTO from '../../Domain/Dtos/Post/LikePostDTO';
import ITagService from '../../Application.Services/Tag/ITagService';
import User from '../../Domain/Models/User/User';
import { createTestPost } from '../Helpers/postUtils';
import * as AddPostRequestHandler from '../../WebApi/Controllers/PostsController/AddPostRequestHandler';
import * as DeletePostRequestHandler from '../../WebApi/Controllers/PostsController/DeletePostRequestHandler';
import * as EditPostRequestHandler from '../../WebApi/Controllers/PostsController/EditPostRequestHandler';
import * as GetPostByPostIdRequestHandler from '../../WebApi/Controllers/PostsController/GetPostByPostIdRequestHandler';
import * as GetPostsByUserIdRequestHandler from '../../WebApi/Controllers/PostsController/GetPostsByUserIdRequestHandler';
import * as getPostByCategoryId from '../../WebApi/Controllers/PostsController/GetPostsByCategoryIdRequestHandler';
import * as GetPostLikesRequestHandler from '../../WebApi/Controllers/PostsController/GetPostLikesRequestHandler';
import * as GetPostUserLikeStatus from '../../WebApi/Controllers/PostsController/GetPostUserLikeStatus';
import * as SetLikeRequestHandler from '../../WebApi/Controllers/PostsController/SetLikeRequestHandler';
import * as SetPostAsUserFavouriteHandler from '../../WebApi/Controllers/PostsController/SetPostAsUserFavouriteHandler';
import IAuthorizationService from '../../Application.Services/Authorization/IAuthorizationService';
import { AuthorizationServiceMOCK } from '../Mocks/AuthorizationService';
import { AuthenticatedRequest } from '../../Domain/Models/User/AuthenticatedRequest';

describe('Post routes', () => {
  let postRoute: PostRoute;
  let postService: IPostService;
  const authorizationService: IAuthorizationService =
    new AuthorizationServiceMOCK();
  const tagService: ITagService = new TagServiceMOCK();
  let container: Container;
  let request: Partial<AuthenticatedRequest>;
  let response: Partial<Response>;
  let sandbox: sinon.SinonSandbox;

  beforeEach((done) => {
    sandbox = sinon.sandbox.create();
    request = {} as AuthenticatedRequest;
    response = {
      send: (body?) => body,
      status: function () {
        return this as Response;
      },
      redirect: () => {
        // Empty body
      }
    };
    container = new Container();
    container.bind<IPostService>(TYPES.IPostService).to(PostServiceMOCK);
    container.bind<ITagService>(TYPES.ITagService).to(TagServiceMOCK);

    postService = container.get(TYPES.IPostService);
    postRoute = new PostRoute(postService, authorizationService, tagService);
    done();
  });

  afterEach(() => {
    sandbox.restore();
  });

  describe('success cases', () => {
    it('should return a response with success property true and msg "Post added", when adding a post succeeds', async () => {
      const testPost = createTestPost();
      const stubHandler = sandbox
        .stub(AddPostRequestHandler, 'handleAddPost')
        .callsFake(() => {
          return Promise.resolve({
            success: true,
            postID: testPost.id,
            msg: 'Post added'
          });
        });
      const spyStatus = sandbox.spy(response, 'status');
      const spySend = sandbox.spy(response, 'send');

      await postRoute.addPost(
        request as AuthenticatedRequest,
        response as Response
      );

      expect(stubHandler.calledOnceWith(request, postService)).to.be.true;
      expect(spyStatus.calledOnceWith(201)).to.be.true;
      expect(
        spySend.calledOnceWith({
          success: true,
          postID: testPost.id,
          msg: 'Post added'
        })
      ).to.be.true;
    });
    it("should return a response with a key posts and as value the user's posts, filtered by his/her id", async () => {
      const userPosts = [createTestPost(), createTestPost(2, 2, [], 2)];
      const postsResponse = Post.toPostArrayResponse(userPosts);
      const stubHandler = sandbox
        .stub(GetPostsByUserIdRequestHandler, 'handleGetPostsByUserId')
        .callsFake(() => {
          return Promise.resolve({ posts: postsResponse });
        });
      const spySend = sandbox.spy(response, 'send');

      await postRoute.getPostsByUserId(
        request as AuthenticatedRequest,
        response as Response
      );

      expect(stubHandler.calledOnceWith(request, response, postService)).to.be
        .true;
      expect(spySend.calledOnceWith({ posts: postsResponse })).to.be.true;
    });

    // it('should return a response a key posts and as value all the posts', async() => {
    //     const userPosts = [createTestPost(), createAnotherTestPost()];
    //     const postsResponse = Post.toPostArrayResponse(userPosts);
    //     const stubHandler = sinon.stub(PostHandlers, "handleGetAllPosts").callsFake(() => {
    //         return Promise.resolve({ posts: postsResponse });
    //     });
    //     const spySend = sinon.spy(response, "send");
    //     const execute = await postRoute.getAllPosts(request, response);

    //     stubHandler.restore();
    //     expect(stubHandler.calledOnceWith(request, response, postService)).to.be.true;
    //     expect(spySend.calledOnceWith({ posts: postsResponse })).to.be.true;
    // });

    it('should return a response with a key post and as value the post with the matching id', async () => {
      const testPost = createTestPost();
      const stubHandler = sandbox
        .stub(GetPostByPostIdRequestHandler, 'handleGetPostByPostId')
        .callsFake(() => {
          return Promise.resolve({ post: Post.toPostResponse(testPost) });
        });
      const spySend = sandbox.spy(response, 'send');

      await postRoute.getPostById(
        request as AuthenticatedRequest,
        response as Response
      );

      expect(stubHandler.calledOnceWith(request, response, postService)).to.be
        .true;
      expect(spySend.calledOnceWith({ post: Post.toPostResponse(testPost) })).to
        .be.true;
    });

    it('should return a response with a key posts and as value the posts filtered by a category id', async () => {
      const userPosts = [createTestPost(), createTestPost(2, 2, [], 2)];
      const stubHandler = sandbox
        .stub(getPostByCategoryId, 'handleGetPostsByCategoryId')
        .callsFake(() => {
          return Promise.resolve({
            posts: Post.toPostArrayResponse(userPosts)
          });
        });
      const spySend = sandbox.spy(response, 'send');

      await postRoute.getPostByCategoryId(
        request as AuthenticatedRequest,
        response as Response
      );

      expect(stubHandler.calledOnceWith(request, response, postService)).to.be
        .true;
      expect(
        spySend.calledOnceWith({ posts: Post.toPostArrayResponse(userPosts) })
      ).to.be.true;
    });

    it('should return a response with property success true and msg "Like set to true/false for post with post id {postId}", when the user\'s post like status is set', async () => {
      const likePostDTO: LikePostDTO = {
        post_id: 1,
        like: true
      };
      const stubHandler = sandbox
        .stub(SetLikeRequestHandler, 'handleSetLikeRequest')
        .callsFake(() => {
          return Promise.resolve({
            success: true,
            msg: `Like set to ${likePostDTO.like} for post with post id ${likePostDTO.post_id}`
          });
        });
      const spySend = sandbox.spy(response, 'send');

      await postRoute.likePost(
        request as AuthenticatedRequest,
        response as Response
      );

      expect(stubHandler.calledOnceWith(request, response, postService)).to.be
        .true;
      expect(
        spySend.calledOnceWith({
          success: true,
          msg: `Like set to ${likePostDTO.like} for post with post id ${likePostDTO.post_id}`
        })
      ).to.be.true;
    });

    it("should return a response with property like with the user's post like status", async () => {
      const stubHandler = sandbox
        .stub(GetPostUserLikeStatus, 'handleGetPostLikeStatusRequest')
        .callsFake(() => {
          return Promise.resolve({ like: true });
        });
      const spySend = sandbox.spy(response, 'send');

      await postRoute.getPostLikeStatus(
        request as AuthenticatedRequest,
        response as Response
      );

      expect(stubHandler.calledOnceWith(request, response, postService)).to.be
        .true;
      expect(spySend.calledOnceWith({ like: true })).to.be.true;
    });

    it("should return a response with property likes with the number of a post's likes", async () => {
      const stubHandler = sandbox
        .stub(GetPostLikesRequestHandler, 'handleGetPostLikesRequest')
        .callsFake(() => {
          return Promise.resolve({ likes: 50 });
        });
      const spySend = sandbox.spy(response, 'send');

      await postRoute.getPostLikes(
        request as AuthenticatedRequest,
        response as Response
      );

      expect(stubHandler.calledOnceWith(request, response, postService)).to.be
        .true;
      expect(spySend.calledOnceWith({ likes: 50 })).to.be.true;
    });

    it('should return a response with the id of the deleted post when deleting a post', async () => {
      const deletedPostId = 1;
      const stubHandler = sandbox
        .stub(DeletePostRequestHandler, 'handleDeletePostRequest')
        .callsFake(() => {
          return Promise.resolve({ success: true, id: deletedPostId });
        });
      const spySend = sandbox.spy(response, 'send');

      await postRoute.deletePostById(
        request as AuthenticatedRequest,
        response as Response
      );

      expect(stubHandler.calledOnceWith(request, response, postService)).to.be
        .true;
      expect(spySend.calledOnceWith({ success: true, id: deletedPostId })).to.be
        .true;
    });

    it('should return a response with a key postId and as value the post id when editing a post succeeds', async () => {
      const stubHandler = sandbox
        .stub(EditPostRequestHandler, 'handleEditPost')
        .callsFake(() => {
          return Promise.resolve(1);
        });
      const spySend = sandbox.spy(response, 'send');

      await postRoute.editPostWithId(
        request as AuthenticatedRequest,
        response as Response
      );

      expect(stubHandler.calledOnceWith(request, response, postService)).to.be
        .true;
      expect(spySend.calledOnceWith({ postId: 1 })).to.be.true;
    });

    it("should return a response with a key postId and as value the post id when setting a post as user's favourite", async () => {
      request.body = { postId: 100 };
      request.user = { id: 1 } as User;
      const stubHandler = sandbox
        .stub(SetPostAsUserFavouriteHandler, 'handleSetPostAsUserFavourite')
        .callsFake(() => {
          return Promise.resolve({ postId: 100 });
        });
      const spySend = sandbox.spy(response, 'send');

      await postRoute.setPostAsUserFavourite(
        request as AuthenticatedRequest,
        response as Response
      );

      expect(stubHandler.calledOnceWith(100, 1, postService)).to.be.true;
      expect(spySend.calledOnceWith({ postId: 100 })).to.be.true;
    });

    it('should return a response with a key tags and as value all the available tags', async () => {
      const mockedTags = [
        { id: 1, name: 'Javascipt' },
        { id: 2, name: 'Typescript' }
      ];
      const stubHandler = sandbox
        .stub(TagHandlers, 'handleGetTags')
        .callsFake(() => {
          return Promise.resolve(mockedTags);
        });
      const spySend = sandbox.spy(response, 'send');
      await postRoute.getTags(
        request as AuthenticatedRequest,
        response as Response
      );

      expect(stubHandler.calledOnceWith(request, response, tagService)).to.be
        .true;
      expect(spySend.calledOnceWith({ tags: mockedTags })).to.be.true;
    });
  });
  describe('error cases', () => {
    it('should handle the error when adding the post fails', async () => {
      const error = { message: 'ErrorMessage', stack: 'ErrorStackTrace' };
      const stubHandler = sandbox
        .stub(AddPostRequestHandler, 'handleAddPost')
        .throws(error);
      const stubHandleError = sandbox.stub(ErrorHandler, 'handleError');

      await postRoute.addPost(
        request as AuthenticatedRequest,
        response as Response
      );

      expect(stubHandleError.calledOnceWith(error, response)).to.be.true;

      stubHandler.restore();
      stubHandleError.restore();
    });

    it('should handle the error when deleting a post fails', async () => {
      const error = { message: 'ErrorMessage', stack: 'ErrorStackTrace' };
      const stubHandler = sandbox
        .stub(DeletePostRequestHandler, 'handleDeletePostRequest')
        .throws(error);
      const stubHandleError = sandbox.stub(ErrorHandler, 'handleError');

      await postRoute.deletePostById(
        request as AuthenticatedRequest,
        response as Response
      );

      expect(stubHandleError.calledOnceWith(error)).to.be.true;

      stubHandler.restore();
      stubHandleError.restore();
    });
    it("should handle the error when trying to edit a post that doesn't exist", async () => {
      const error = { message: 'ErrorMessage', stack: 'ErrorStackTrace' };
      const stubHandler = sandbox
        .stub(EditPostRequestHandler, 'handleEditPost')
        .throws(error);
      const stubHandleError = sandbox.stub(ErrorHandler, 'handleError');

      await postRoute.editPostWithId(
        request as AuthenticatedRequest,
        response as Response
      );

      expect(stubHandleError.calledOnceWith(error)).to.be.true;

      stubHandler.restore();
      stubHandleError.restore();
    });

    it('should handle the error when setting a post as favourite fails', async () => {
      request.body = { postId: 100 };
      request.user = { id: 1 } as User;
      const error = { message: 'ErrorMessage', stack: 'ErrorStackTrace' };
      const stubHandler = sandbox
        .stub(SetPostAsUserFavouriteHandler, 'handleSetPostAsUserFavourite')
        .throws(error);
      const stubHandleError = sandbox.stub(ErrorHandler, 'handleError');

      await postRoute.setPostAsUserFavourite(
        request as AuthenticatedRequest,
        response as Response
      );

      expect(stubHandleError.calledOnceWith(error, response)).to.be.true;

      stubHandler.restore();
      stubHandleError.restore();
    });

    it('should set a response with the errorMessage "Something went wrong when retrieving tags" when getting the tags fails', async () => {
      const stubHandler = sandbox.stub(TagHandlers, 'handleGetTags').throws();
      const spySend = sandbox.spy(response, 'send');

      await postRoute.getTags(
        request as AuthenticatedRequest,
        response as Response
      );

      expect(stubHandler.calledOnceWith(request, response, tagService)).to.be
        .true;
      expect(
        spySend.calledOnceWith({
          errorMessage: 'Something went wrong when retrieving tags'
        })
      ).to.be.true;
    });
  });
});
