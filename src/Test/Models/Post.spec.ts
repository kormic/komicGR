import * as chai from 'chai';
import PostResponseDTO from '../../Domain/Dtos/Post/PostResponseDTO';
const expect = chai.expect;

import Post from '../../Domain/Models/Post/Post';
import Tag from '../../Domain/Models/Tag/Tag';

import {
  createTestPostWithNoTitle,
  createTestPostWithNoUserId,
  createTestPostWithNoBody,
  createTestPostOnlyWithTitleAndBody,
  createTestPost
} from '../Helpers/postUtils';

describe('Post model', () => {
  it('should throw an error if the post title is empty', () => {
    expect(createTestPostWithNoTitle).to.throw('A post should have a title');
  });

  it('should throw an error if the user id is empty', () => {
    expect(createTestPostWithNoUserId).to.throw('A post should have a user id');
  });

  it('should throw an error if the post body is empty', () => {
    expect(createTestPostWithNoBody).to.throw('A post should have a body text');
  });

  it('should create a post with 0 likes and a short body created by the 20 first characters of body, if the short body and likes are null', () => {
    const post = createTestPostOnlyWithTitleAndBody();

    expect(post.likes).to.equal(0);
    expect(post.short_body).to.equal(post.body.substring(0, 20));
  });

  it('should have a static function to convert Post to PostResponseDTO', () => {
    const post = createTestPost();
    const expected: PostResponseDTO = {
      id: post.id,
      title: post.title,
      user_id: post.user_id,
      short_body: post.short_body,
      body: post.body,
      categoryId: post.category_id,
      likes: post.likes,
      profileImageUrl: post.profile_image_url,
      createdAt: post.created_at,
      imageUrl: post.image_url,
      tags: post.tags
    };
    expect(Post.toPostResponse(post)).to.deep.equal(expected);
  });

  it('should have an empty array of tags', () => {
    expect(createTestPost().tags.length).to.equal(0);
  });

  it('should have a static function to convert Post to PostArrayResponseDTO', () => {
    const post = createTestPost();
    const expected: PostResponseDTO[] = [
      {
        id: post.id,
        title: post.title,
        user_id: post.user_id,
        short_body: post.short_body,
        body: post.body,
        categoryId: post.category_id,
        likes: post.likes,
        profileImageUrl: post.profile_image_url,
        createdAt: post.created_at,
        imageUrl: post.image_url,
        tags: post.tags
      }
    ];
    expect(Post.toPostArrayResponse([post])).to.deep.equal(expected);
  });

  it('should have a static function to create a Post from the query result', () => {
    const post = createTestPost(undefined, undefined, [
      new Tag(1, 'Javascript')
    ]);
    const queryResult = {
      id: String(post.id),
      title: post.title,
      user_id: post.user_id.toString(),
      short_body: post.short_body,
      body: post.body,
      category_id: post.category_id!.toString(),
      likes: post.likes.toString(),
      profile_image_url: post.profile_image_url,
      created_at: post.created_at,
      image_url: post.image_url,
      tags: `{ "id": "${post.tags[0].id.toString()}", "name": "${
        post.tags[0].name
      }" }`
    };
    const expectedPost = Post.createPostFromQueryResult(queryResult);
    expect(expectedPost).to.deep.equal(post);
  });

  it('should convert the Post model to the correct PostResponseDTO', () => {
    const testPost = createTestPost(undefined, undefined, undefined, 1);
    expect(Post.toPostResponse(testPost)).to.deep.equal({
      id: testPost.id,
      title: testPost.title,
      short_body: testPost.short_body,
      body: testPost.body,
      likes: testPost.likes,
      user_id: testPost.user_id,
      createdAt: testPost.created_at,
      imageUrl: testPost.image_url,
      profileImageUrl: testPost.profile_image_url,
      tags: testPost.tags,
      categoryId: testPost.category_id
    });
  });

  it.skip('should convert a Post to a PostResponse correctly');
});
