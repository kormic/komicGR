import { injectable } from 'inversify';

import IAuthorizationService from '../../Application.Services/Authorization/IAuthorizationService';

@injectable()
export class AuthorizationServiceMOCK implements IAuthorizationService {
  getCanAddPost(): boolean {
    throw new Error('Method not implemented.');
  }
}
