import { injectable } from "inversify";

import ICategoryService from "../../Application.Services/Category/ICategoryService";

import AddCategoryDTO from "../../Domain/Dtos/Category/AddCategoryDTO";
import Category from "../../Domain/Models/Category/Category";

@injectable()
export class CategoryServiceMOCK implements ICategoryService {
    addCategory(newCategory: AddCategoryDTO): Promise<any> {
        throw new Error("Method not implemented.");
    }    
    
    setPostCategory(postId: number, categoryId: number): Promise<any> {
        throw new Error("Method not implemented.");
    }
    getPostCategories(): Promise<Category[]> {
        throw new Error("Method not implemented.");
    }
}