import Role from '../../Domain/Models/Role/Role';
import User from '../../Domain/Models/User/User';

export const createUser = ({
  id = 1,
  first_name = 'test',
  last_name = 'last',
  username = 'test',
  password = '12345678',
  role = Role.Human,
  email = 'test@test.com',
  job_desc = 'job',
  address = 'address',
  mobile = 123456677,
  profile_image_url = '',
  registration_date = new Date(),
  confirmed = false
} = {}) => {
  const user: User = {
    id,
    first_name,
    last_name,
    username,
    password,
    role,
    email,
    job_desc,
    address,
    mobile,
    profile_image_url,
    registration_date,
    confirmed
  };

  return user;
};
