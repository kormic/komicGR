import * as sinon from 'sinon';
import * as chai from "chai";
const expect = chai.expect;

import * as nodemailer from 'nodemailer';

import NodeMailerWrapper from '../../Helpers/NodeMailer/NodeMailerWrapper';

import MailOptions from '../../Domain/Dtos/MailOptionsDTO';

const transporterOptions = {
    host: 'mail.test.host',
    port: 465,
    secure: true, // true for 465, false for other ports
    auth: {
        user: "user@email.gr", // generated ethereal user
        pass: "userpass" // generated ethereal password
    },
    tls: {
        rejectUnauthorized: false
    }
};
const transporter = nodemailer.createTransport(transporterOptions);

describe('Node Mailer Factory', () => {
    const nodeMailer = new NodeMailerWrapper(transporterOptions);

    it('should instantiate node mailer properly', () => {
        expect(nodeMailer).to.be.a('object');
        expect(nodeMailer.getTransporter).to.be.a('function');
        expect(nodeMailer.getTransporter().options).to.deep.equal(transporter.options);
    })

    it('should send a mail properly by calling node mailer\'s sendEmail function', () => {
        const transporter = nodeMailer.getTransporter();
        const sendEmailStub = sinon.stub(transporter, 'sendMail');
        const mailOptions: MailOptions = {
            from: '"Test 👻" <info@test.gr>', // sender address
            to: 'test@test.com', // list of receivers
            subject: 'Hello ✔', // Subject line
            text: 'Hello world?', // plain text body
            html: '<b>Hello world?</b>' // html body
        };
        nodeMailer.sendEmail(mailOptions);
        
        expect(nodeMailer.sendEmail).to.be.a('function');
        expect(sendEmailStub.called).to.be.true;
        expect(sendEmailStub.calledWith(mailOptions)).to.be.true;
    })
});