import sinon from 'sinon';
import chai from 'chai';
import { Request, Response } from 'express';
const expect = chai.expect;

import BcryptService from '../../Helpers/Bcryptjs/BcryptService';

describe('Bcrypt service', () => {
    const bcryptService = BcryptService;

    it('should generate a salt properly', async () => {
        const saltPromise = BcryptService.generateSalt(10);
        const salt = await saltPromise;

        expect(saltPromise).to.be.instanceOf(Promise);
        expect(salt).to.be.a('string');
    });

    it('should generate a haskey properly', async () => {
        const someValue = 'test';
        const salt = await BcryptService.generateSalt(10);
        const hashPromise = BcryptService.createHashKey(someValue, salt);

        const hashkey = await hashPromise;

        expect(hashPromise).to.be.instanceOf(Promise);
        expect(hashkey).to.be.a('string');
        expect(hashkey).not.equal(someValue);
    });

    it('should compare passwords properly', async () => {
        const aPassword = 'password';
        const salt = await BcryptService.generateSalt(10);
        const hashkey = await BcryptService.createHashKey(aPassword, salt)
        const comparePromise = BcryptService.comparePassword(aPassword, hashkey);

        const isMatch = await comparePromise;

        expect(comparePromise).to.be.instanceOf(Promise);
        expect(isMatch).to.be.a('boolean');
        expect(isMatch).to.equal(true);
    });

    // it('should send a mail properly by calling node mailer\'s sendEmail function', () => {
    //     const transporter = nodeMailer.getTransporter();
    //     const sendEmailStub = sinon.stub(transporter, 'sendMail');
    //     const mailOptions: MailOptions = {
    //         from: '"Test 👻" <info@test.gr>', // sender address
    //         to: 'test@test.com', // list of receivers
    //         subject: 'Hello ✔', // Subject line
    //         text: 'Hello world?', // plain text body
    //         html: '<b>Hello world?</b>' // html body
    //     };
    //     nodeMailer.sendEmail(mailOptions);

    //     expect(nodeMailer.sendEmail).to.be.a('function');
    //     expect(sendEmailStub.called).to.be.true;
    //     expect(sendEmailStub.calledWith(mailOptions)).to.be.true;
    // })
});