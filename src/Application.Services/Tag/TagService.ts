import { injectable, inject } from 'inversify';

import Tag from '@models/Tag/Tag';
import TYPES from '@helpers/DI/Types';
import ITagRepository from '@repos/Tag/ITagRepository';
import ITagService from './ITagService';

@injectable()
class TagService implements ITagService {
  constructor(
    @inject(TYPES.ITagRepository) private tagRepository: ITagRepository
  ) {}

  getTags(): Promise<Tag[]> {
    return this.tagRepository.getTags();
  }
}

export default TagService;
