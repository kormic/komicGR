import Tag from '@models/Tag/Tag';

interface ITagService {
  getTags(): Promise<Tag[]>;
}

export default ITagService;
