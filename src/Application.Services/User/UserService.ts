import { injectable, inject } from 'inversify';

import User from '@models/User/User';
import TYPES from '@helpers/DI/Types';
import BcryptService from '@helpers/Bcryptjs/BcryptService';
import RegisterUserDTO from '@dtos/User/RegisterUserDTO';
import { QueryResponse } from '@dtos/QueryResponse';
import IUserRepository from '@repos/User/IUserRepository';
import IUserService from './IUserService';

@injectable()
class UserService implements IUserService {
  constructor(
    @inject(TYPES.IUserRepository) private userRepository: IUserRepository,
    @inject(TYPES.BcryptService) private bcryptService: BcryptService
  ) {}

  public getAll(): Promise<User[]> {
    return this.userRepository.getAll();
  }

  public getById(id: number): Promise<User> {
    return this.userRepository.getById(id);
  }

  public getByUsername(username: string): Promise<User> {
    return this.userRepository.getByUsername(username);
  }

  public getByEmail(email: string): Promise<User> {
    return this.userRepository.getByEmail(email);
  }

  public getUserByPostId(postId: number): Promise<User> {
    return this.userRepository.getUserByPostId(postId);
  }

  public async insertUser(user: RegisterUserDTO): Promise<QueryResponse> {
    const salt = await BcryptService.generateSalt(10);
    const hashKey = await BcryptService.createHashKey(user.password, salt);
    user.password = hashKey;

    const insertResult = await this.userRepository.insertUser(user);

    return insertResult;
  }

  public setProfileImageUrl(
    userId: number,
    profileImageUrl: string
  ): Promise<boolean> {
    return this.userRepository.setProfileImageUrl(userId, profileImageUrl);
  }

  public setConfirmedEmail(
    userId: number,
    confirmed: boolean
  ): Promise<boolean> {
    return this.userRepository.setConfirmedEmail(userId, confirmed);
  }

  public async comparePassword(
    candidatePassword: string,
    hash: string
  ): Promise<any> {
    const isMatch = await BcryptService.comparePassword(
      candidatePassword,
      hash
    );

    return isMatch;
  }

  public updatePassword(userId: number, newPassword: string): Promise<boolean> {
    return this.userRepository.updatePassword(userId, newPassword);
  }
}

export default UserService;
