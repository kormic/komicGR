import RegisterUserDTO from '@dtos/User/RegisterUserDTO';
import { QueryResponse } from '@dtos/QueryResponse';

interface IUserService {
  getAll(): Promise<any>;
  getById(id: number): Promise<any>;
  getByUsername(username: string): Promise<any>;
  getByEmail(email: string): Promise<any>;
  getUserByPostId(postId: number): Promise<any>;
  insertUser(user: RegisterUserDTO): Promise<QueryResponse>;
  setProfileImageUrl(userId: number, profileImageUrl: string): Promise<boolean>;
  // insertUserCallback(user: User, callback: Function);
  setConfirmedEmail(userId: number, confirmed: boolean): Promise<boolean>;
  comparePassword(candidatePassword: string, hash: string): Promise<any>;
  updatePassword(userId: number, newPassword: string): Promise<boolean>;
}

export default IUserService;
