import AddCategoryDTO from '@dtos/Category/AddCategoryDTO';
import Category from '@models/Category/Category';

interface ICategoryService {
  addCategory(newCategory: AddCategoryDTO): Promise<any>;
  setPostCategory(postId: number, categoryId: number): Promise<any>;
  getPostCategories(): Promise<Category[]>;
}

export default ICategoryService;
