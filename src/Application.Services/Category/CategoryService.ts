import { injectable, inject } from 'inversify';

import AddCategoryDTO from '@dtos/Category/AddCategoryDTO';
import Category from '@models/Category/Category';
import TYPES from '@helpers/DI/Types';
import ICategoryService from './ICategoryService';
import ICategoryRepository from '@repos/Category/ICategoryRepository';

@injectable()
class CategoryService implements ICategoryService {
  constructor(
    @inject(TYPES.ICategoryRepository)
    private categoryRepository: ICategoryRepository
  ) {}

  addCategory(newCategory: AddCategoryDTO): Promise<any> {
    return this.categoryRepository.addCategory(newCategory);
  }

  setPostCategory(postId: number, categoryId: number): Promise<any> {
    return this.categoryRepository.setPostCategory(postId, categoryId);
  }

  getPostCategories(): Promise<Category[]> {
    return this.categoryRepository.getPostCategories();
  }
}

export default CategoryService;
