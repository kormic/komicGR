import { injectable, inject } from 'inversify';

import Post from '@models/Post/Post';
import TYPES from '@helpers/DI/Types';
import { QueryResponse } from '@dtos/QueryResponse';
import AddPostDTO from '@dtos/Post/AddPostDTO';
import EditPostDTO from '@dtos/Post/EditPostDTO';
import IPostRepository from '@repos/Post/IPostRepository';
import IPostService from './IPostService';

@injectable()
class PostService implements IPostService {
  constructor(
    @inject(TYPES.IPostRepository) private postRepository: IPostRepository
  ) {}

  getAllPosts(): Promise<Post[]> {
    return this.postRepository.getAllPosts();
  }

  getPostByPostId(postId: number): Promise<Post> {
    return this.postRepository.getPostByPostId(postId);
  }

  getPostsByUserId(
    userId: number,
    offset?: number,
    limit?: number
  ): Promise<Post[]> {
    return this.postRepository.getPostsByUserId(userId, offset, limit);
  }

  getPostsByCategoryId(
    categoryId: number,
    offset?: number,
    limit?: number
  ): Promise<Post[]> {
    return this.postRepository.getPostsByCategoryId(categoryId, offset, limit);
  }

  addPost(newPost: AddPostDTO): Promise<QueryResponse> {
    return this.postRepository.addPost(newPost);
  }

  likePost(userId: number, postId: number, like: boolean): Promise<boolean> {
    return this.postRepository.likePost(userId, postId, like);
  }

  getIfUserLikesPostByPostId(userId: number, postId: number): Promise<boolean> {
    return this.postRepository.getIfUserLikesPostByPostId(userId, postId);
  }

  getNumberOfPostLikes(postId: number): Promise<number> {
    return this.postRepository.getNumberOfPostLikes(postId);
  }

  deletePostById(userId: number, postId: number): Promise<boolean> {
    return this.postRepository.deletePostByPostId(postId);
  }

  editPost(postId: number, editPostDTO: EditPostDTO): Promise<{ id: number }> {
    return this.postRepository.editPost(postId, editPostDTO);
  }

  setPostAsUserFavourite(
    postId: number,
    userId: number
  ): Promise<{ postId: number }> {
    return this.postRepository.setPostAsUserFavourite(postId, userId);
  }
}

export default PostService;
