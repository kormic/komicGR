import User from '@models/User/User';

interface IAuthorizationService {
  getCanAddPost(user: User): boolean;
}

export default IAuthorizationService;
