import { expect } from 'chai';

import Role from '@models/Role/Role';
import { createUser } from '../../Test/Helpers/user';
import { AuthorizationService } from './AuthorizationService';

describe('Authorization Service', () => {
  describe('getCanAddPost', () => {
    it('should return return false for a user with the Role Human', () => {
      const user = createUser();
      const authorizationService = new AuthorizationService();

      const canAddPost = authorizationService.getCanAddPost(user);

      expect(canAddPost).to.be.false;
    });

    it('should return true for a user with the Role SuperHuman', () => {
      const user = createUser({ role: Role.SuperHuman });
      const authorizationService = new AuthorizationService();

      const canAddPost = authorizationService.getCanAddPost(user);

      expect(canAddPost).to.be.true;
    });

    it('should return true for a user with the Role God', () => {
      const user = createUser({ role: Role.SuperHuman });
      const authorizationService = new AuthorizationService();

      const canAddPost = authorizationService.getCanAddPost(user);

      expect(canAddPost).to.be.true;
    });
  });
});
