import { injectable } from 'inversify';

import Role from '@models/Role/Role';
import User from '@models/User/User';
import IAuthorizationService from './IAuthorizationService';

@injectable()
export class AuthorizationService implements IAuthorizationService {
  getCanAddPost(user: User) {
    return user.role !== Role.Human;
  }
}
