enum Role {
  Human = 'Human',
  SuperHuman = 'SuperHuman',
  God = 'God'
}

export default Role;
