enum RouteUrl {
    Users = 0,
    Posts,
    Root,
    Categories
}

export default RouteUrl