import ProfileUserResponseDTO from '@dtos/User/ProfileUserResponseDTO';
import Role from '../Role/Role';

class User {
  constructor(
    public id: number,
    public role: Role,
    public first_name: string,
    public last_name: string,
    public email: string,
    public username: string,
    public password: string,
    public job_desc: string,
    public address: string,
    public mobile: number,
    public profile_image_url: string,
    public registration_date: Date,
    public confirmed: boolean = false
  ) {
    if (!username) throw new Error('Username cannot be empty');

    if (!password) throw new Error('Password cannot be empty');

    if (!email) throw new Error('Email cannot be empty');
  }

  static toProfileResponse(user: User): ProfileUserResponseDTO {
    return {
      id: user.id,
      role: user.role,
      first_name: user.first_name,
      last_name: user.last_name,
      email: user.email,
      username: user.username,
      job_desc: user.job_desc,
      address: user.address,
      mobile: user.mobile,
      profile_image_url: user.profile_image_url,
      registration_date: user.registration_date.toDateString()
    };
  }

  static toProfileResponseArray(users: User[]): ProfileUserResponseDTO[] {
    const profiles: ProfileUserResponseDTO[] = [];

    for (const user of users) {
      profiles.push(User.toProfileResponse(user));
    }

    return profiles;
  }
}

export default User;
