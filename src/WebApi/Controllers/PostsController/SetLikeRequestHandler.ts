import { Response } from 'express';

import { AuthenticatedRequest } from '@models/User/AuthenticatedRequest';
import IPostService from '@services/Post/IPostService';
import LikePostDTO from '@dtos/Post/LikePostDTO';

export async function handleSetLikeRequest(
  req: AuthenticatedRequest,
  res: Response,
  postService: IPostService
): Promise<{ success: boolean; msg: string }> {
  const likePostDTO = <LikePostDTO>req.body;

  const result = await postService.likePost(
    req.user.id,
    likePostDTO.post_id,
    likePostDTO.like
  );

  return {
    success: result,
    msg: `Like set to ${likePostDTO.like} for post with post id ${likePostDTO.post_id}`
  };
}
