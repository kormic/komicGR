import { Response } from 'express';

import { AuthenticatedRequest } from '@models/User/AuthenticatedRequest';
import IPostService from '@services/Post/IPostService';
import EditPostDTO from '@dtos/Post/EditPostDTO';

export async function handleEditPost(
  req: AuthenticatedRequest,
  res: Response,
  postService: IPostService
): Promise<number> {
  const editPostDTO = req.body as EditPostDTO;
  const postId = +req.params.postId;
  const { user_id: userId } = await postService.getPostByPostId(postId);

  if (req.user.id !== userId) {
    throw new Error('You are not authorized to edit this post');
  }

  if (!editPostDTO.title) {
    throw new Error('Required fields are missing');
  }

  const result = await postService.editPost(+postId, editPostDTO);

  return result.id;
}
