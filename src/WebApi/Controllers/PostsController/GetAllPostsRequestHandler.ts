import { Request, Response } from 'express';

import IPostService from '@services/Post/IPostService';
import Post from '@models/Post/Post';
import PostResponseDTO from '@dtos/Post/PostResponseDTO';

export async function handleGetAllPosts(
  req: Request,
  res: Response,
  postService: IPostService
): Promise<{ posts: PostResponseDTO[] }> {
  const posts = await postService.getAllPosts();

  return { posts: Post.toPostArrayResponse(posts) };
}
