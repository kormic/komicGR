import { Request, Response } from 'express';

import IPostService from '@services/Post/IPostService';
import PostResponseDTO from '@dtos/Post/PostResponseDTO';
import Post from '@models/Post/Post';

export async function handleGetPostByPostId(
  req: Request,
  res: Response,
  postService: IPostService
): Promise<{ post: PostResponseDTO }> {
  const post = await postService.getPostByPostId(+req.params.postId);

  return { post: Post.toPostResponse(post) };
}
