import { Response } from 'express';

import { AuthenticatedRequest } from '@models/User/AuthenticatedRequest';
import IPostService from '@services/Post/IPostService';

export async function handleGetPostLikeStatusRequest(
  req: AuthenticatedRequest,
  res: Response,
  postService: IPostService
): Promise<{ like: boolean }> {
  const result = await postService.getIfUserLikesPostByPostId(
    req.user.id,
    +req.params.postId
  );

  return { like: result };
}
