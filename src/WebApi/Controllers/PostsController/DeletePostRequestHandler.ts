import { Response } from 'express';

import { AuthenticatedRequest } from '@models/User/AuthenticatedRequest';
import IPostService from '@services/Post/IPostService';

export async function handleDeletePostRequest(
  req: AuthenticatedRequest,
  res: Response,
  postService: IPostService
): Promise<{ success: boolean; msg: string; postId?: number }> {
  const post = await postService.getPostByPostId(+req.params.postId);

  if (!post) {
    return { success: false, msg: 'Post does not exist' };
  }

  if (post.user_id !== +req.user.id) {
    return { success: false, msg: 'You are not allowed to delete this post' };
  }

  const result = await postService.deletePostById(
    req.user.id,
    +req.params.postId
  );

  if (result) {
    return { success: true, msg: 'Post deleted', postId: +req.params.postId };
  }

  return { success: false, msg: 'Something went wrong' };
}
