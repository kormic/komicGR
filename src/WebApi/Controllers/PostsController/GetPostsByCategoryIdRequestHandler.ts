import { Request, Response } from 'express';

import IPostService from '@services/Post/IPostService';
import PostResponseDTO from '@dtos/Post/PostResponseDTO';
import Post from '@models/Post/Post';

export async function handleGetPostsByCategoryId(
  req: Request,
  res: Response,
  postService: IPostService
): Promise<{ posts: PostResponseDTO[] }> {
  const posts = await postService.getPostsByCategoryId(
    +req.query.categoryId!,
    +req.query.offset!,
    +req.query.limit!
  );

  return { posts: Post.toPostArrayResponse(posts) };
}
