import IPostService from '@services/Post/IPostService';

export async function handleSetPostAsUserFavourite(
  postId: number,
  userId: number,
  postService: IPostService
): Promise<{ postId: number }> {
  const result = await postService.setPostAsUserFavourite(postId, userId);

  return result;
}
