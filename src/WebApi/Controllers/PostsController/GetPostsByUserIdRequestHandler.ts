import { Request, Response } from 'express';

import IPostService from '@services/Post/IPostService';
import Post from '@models/Post/Post';
import PostResponseDTO from '@dtos/Post/PostResponseDTO';

export async function handleGetPostsByUserId(
  req: Request,
  res: Response,
  postService: IPostService
): Promise<{ posts: PostResponseDTO[] }> {
  const posts = await postService.getPostsByUserId(
    +req.params.userId,
    +req.query.offset!,
    +req.query.limit!
  );

  return { posts: Post.toPostArrayResponse(posts) };
}
