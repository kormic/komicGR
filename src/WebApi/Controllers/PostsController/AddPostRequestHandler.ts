import AddPostDTO from '@dtos/Post/AddPostDTO';
import IPostService from '@services/Post/IPostService';
import IAuthorizationService from '@services/Authorization/IAuthorizationService';
import { AuthenticatedRequest } from '@models/User/AuthenticatedRequest';

export async function handleAddPost(
  req: AuthenticatedRequest,
  postService: IPostService,
  authorizationService: IAuthorizationService
): Promise<{ success: boolean; postID?: number; msg: string }> {
  if (authorizationService.getCanAddPost(req.user)) {
    const newPost = <AddPostDTO>req.body;

    if (
      !newPost.user_id ||
      !newPost.title ||
      !newPost.body ||
      !newPost.short_body ||
      !newPost.categoryId
    ) {
      return { success: false, msg: 'Required fields are missing' };
    }

    if (newPost.title.length < 3) {
      return {
        success: false,
        msg: 'Title is too short. It should be at least 3 characters'
      };
    }

    const result = await postService.addPost(newPost);

    return { success: true, postID: result.insertId, msg: 'Post added' };
  } else {
    throw { stack: null, message: 'You are not allowed to add a post' };
  }
}
