import { Response } from 'express';

import SetCategoryDTO from '@dtos/Category/SetCategoryDTO';
import ICategoryService from '@services/Category/ICategoryService';
import IPostService from '@services/Post/IPostService';
import Role from '@models/Role/Role';
import { AuthenticatedRequest } from '@models/User/AuthenticatedRequest';

export async function handleSetPostCategory(
  req: AuthenticatedRequest,
  res: Response,
  categoryService: ICategoryService,
  postService: IPostService
): Promise<{ success: boolean; msg: string }> {
  const setCategoryDTO = <SetCategoryDTO>req.body;
  const post = await postService.getPostByPostId(setCategoryDTO.Post_id);

  if (post.user_id != req.user.id) {
    throw { message: "You are not allowed to set this post's category" };
  }

  if (req.user.role === Role.Human) {
    throw { message: 'You are not authorized for this action' };
  }

  await categoryService.setPostCategory(
    setCategoryDTO.Post_id,
    setCategoryDTO.Category_id
  );
  return { success: true, msg: 'Post Category Set' };
}
