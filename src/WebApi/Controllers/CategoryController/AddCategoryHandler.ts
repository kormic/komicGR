import { Response } from 'express';

import AddCategoryDTO from '@dtos/Category/AddCategoryDTO';
import Role from '@models/Role/Role';
import ICategoryService from '@services/Category/ICategoryService';
import { AuthenticatedRequest } from '@models/User/AuthenticatedRequest';

export async function handleAddCategory(
  req: AuthenticatedRequest,
  res: Response,
  categoryService: ICategoryService
): Promise<{ success: boolean; msg: string }> {
  const newCategory = <AddCategoryDTO>req.body;
  const user = req.user;

  if (Role[user.role.toString()] === Role[Role.Human]) {
    throw { message: 'You are not authorized for this action' };
  }

  await categoryService.addCategory(newCategory);

  return { success: true, msg: 'Category added' };
}
