import { Request, Response } from 'express';

import ICategoryService from '@services/Category/ICategoryService';
import Category from '@models/Category/Category';

export async function handleGetPostCategories(
  req: Request,
  res: Response,
  categoryService: ICategoryService
): Promise<{ success: boolean; categories: Category[] }> {
  const categoriesResult = await categoryService.getPostCategories();

  return { success: true, categories: categoriesResult };
}
