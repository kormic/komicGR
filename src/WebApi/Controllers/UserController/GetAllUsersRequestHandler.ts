import { Request, Response } from 'express';

import User from '@models/User/User';
import IUserService from '@services/User/IUserService';
import ProfileUserResponseDTO from '@dtos/User/ProfileUserResponseDTO';

export const handleGetAllUsersRequest = async (
  _: Request,
  __: Response,
  userService: IUserService
): Promise<ProfileUserResponseDTO[]> => {
  const users = await userService.getAll();

  return User.toProfileResponseArray(users);
};
