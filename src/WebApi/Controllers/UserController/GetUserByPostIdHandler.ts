import { Request, Response } from 'express';

import User from '@models/User/User';
import IUserService from '@services/User/IUserService';
import ProfileUserResponseDTO from '@dtos/User/ProfileUserResponseDTO';

export async function handelGetUserByPostId(
  req: Request,
  res: Response,
  userService: IUserService
): Promise<{ success: boolean; user: ProfileUserResponseDTO }> {
  const user = await userService.getUserByPostId(+req.params.postId);

  return { success: true, user: User.toProfileResponse(user) };
}
