import { Response } from 'express';

import { AuthenticatedRequest } from '@models/User/AuthenticatedRequest';
import IUserService from '@services/User/IUserService';

export async function handleResetPasswordRequest(
  req: AuthenticatedRequest,
  res: Response,
  userService: IUserService
): Promise<{ success: boolean }> {
  const userId = req.user.id;
  const newPassword = req.body.newPassword;

  if (newPassword.length < 8) {
    throw new Error('A password must have at least 8 characters');
  }

  const response = await userService.updatePassword(userId, newPassword);

  // TODO: send an email to notify the user that the password has been reset.
  // Also the user should be logged out the next time (s)he visits the app

  return { success: response };
}
