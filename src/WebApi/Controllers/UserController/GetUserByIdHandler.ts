import { Response, Request } from 'express';

import User from '@models/User/User';
import IUserService from '@services/User/IUserService';

export async function handleGetUserById(
  req: Request,
  _: Response,
  userService: IUserService
) {
  const user = await userService.getById(+req.params.id);

  return User.toProfileResponse(user);
}
