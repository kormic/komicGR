import { Response } from 'express';

import { AuthenticatedRequest } from '@models/User/AuthenticatedRequest';
import ProfileUserResponseDTO from '@dtos/User/ProfileUserResponseDTO';

export function handleProfileRequest(
  req: AuthenticatedRequest,
  _: Response
): { user: ProfileUserResponseDTO } {
  return {
    user: {
      ...req.user,
      registration_date: req.user.registration_date.toString()
    }
  };
}
