import { Response } from 'express';

import { AuthenticatedRequest } from '@models/User/AuthenticatedRequest';
import IUserService from '@services/User/IUserService';

export async function handleUploadProfileImageUrlRequest(
  req: AuthenticatedRequest,
  res: Response,
  userService: IUserService
): Promise<{ success: boolean; msg: string }> {
  const updated = await userService.setProfileImageUrl(
    req.user.id,
    req.body.profileImageUrl
  );

  return { success: updated, msg: 'Profile Image Url added succesfully' };
}
