import { Response } from 'express';
import multer from 'multer';

import IUserService from '@services/User/IUserService';
import { AuthenticatedRequest } from '@models/User/AuthenticatedRequest';

export async function handleUploadProfileImageRequest(
  req: AuthenticatedRequest,
  res: Response,
  userService: IUserService,
  multerFactory: any,
  storageOptions: multer.DiskStorageOptions,
  promisifyMulter: any
): Promise<{
  success?: boolean;
  msg?: string;
  errorMessage?: string;
  errorStackTrace?: any;
}> {
  const storageOptionsClone = Object.assign({}, storageOptions);
  storageOptionsClone.destination = (storageOptions.destination as string)
    .concat(String(req.user.id))
    .concat('/');
  const createMulterInstance = multerFactory(storageOptionsClone);

  await promisifyMulter(createMulterInstance().single('userFile'), req, res);

  const profileImageUrl =
    '/profile-images/' +
    req.user.id +
    '/profile.' +
    req.file?.originalname.split('.')[1];

  const updated = await userService.setProfileImageUrl(
    req.user.id,
    profileImageUrl
  );

  return { success: updated, msg: 'Profile Image added succesfully' };
}
