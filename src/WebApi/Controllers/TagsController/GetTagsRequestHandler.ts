import { Request, Response } from 'express';

import ITagService from '@services/Tag/ITagService';
import Tag from '@models/Tag/Tag';

export async function handleGetTags(
  _: Request,
  __: Response,
  tagService: ITagService
): Promise<Tag[]> {
  const tags = await tagService.getTags();

  return tags;
}
