import { Response } from 'express';
import { controller, httpGet } from 'inversify-express-utils';

import ProfileUserResponseDTO from '@dtos/User/ProfileUserResponseDTO';
import { AuthenticatedRequest } from '@models/User/AuthenticatedRequest';
import Role from '@models/Role/Role';
import { ROUTES } from '@constants';

@controller(`/${ROUTES.API_VERSION.V2}/users`)
export class UserRouteV2 {
  constructor() {
    return;
  }

  @httpGet('/')
  async getAllUsers(req: AuthenticatedRequest, res: Response) {
    const dummyUser: ProfileUserResponseDTO[] = [
      {
        id: 1,
        role: Role.God,
        first_name: 'Version 2 first name',
        last_name: 'Version 2 last name',
        email: 'version2@user.com',
        username: 'komic',
        job_desc: 'job description',
        address: 'somewhere',
        mobile: 1244342,
        profile_image_url: 'image url',
        registration_date: '01/01/2021'
      },
      {
        id: 2,
        role: Role.Human,
        first_name: 'Version 2 second first name',
        last_name: 'Version 2 second last name',
        email: 'version2second@user.com',
        username: 'komic second',
        job_desc: 'job description second ',
        address: 'somewhere',
        mobile: 1244333232,
        profile_image_url: 'image url second',
        registration_date: '01/01/2021'
      }
    ];

    res.send(dummyUser);
  }
}
