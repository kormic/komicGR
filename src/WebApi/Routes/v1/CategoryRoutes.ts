import passport from 'passport';
import { httpPost, httpGet, controller } from 'inversify-express-utils';
import { Response } from 'express';
import { inject } from 'inversify';

import {
  handleAddCategory,
  handleSetPostCategory,
  handleGetPostCategories
} from '@controllers/CategoryController/index';
import ICategoryService from '@services/Category/ICategoryService';
import IPostService from '@services/Post/IPostService';
import { AuthenticatedRequest } from '@models/User/AuthenticatedRequest';
import TYPES from '@helpers/DI/Types';
import { ROUTES } from '@constants';
import { handleError } from './utils';

@controller(`/${ROUTES.API_VERSION.V1}/categories`)
export class CategoryRoute {
  constructor(
    @inject(TYPES.ICategoryService) private categoryService: ICategoryService,
    @inject(TYPES.IPostService) private postService: IPostService
  ) {}

  @httpGet('/')
  async getPostCategories(req: AuthenticatedRequest, res: Response) {
    try {
      const response = await handleGetPostCategories(
        req,
        res,
        this.categoryService
      );

      res.send(response);
    } catch (err) {
      return handleError(err, res);
    }
  }

  @httpPost('/add', passport.authenticate('jwt', { session: false }))
  async addCategory(req: AuthenticatedRequest, res: Response) {
    try {
      const response = await handleAddCategory(req, res, this.categoryService);

      res.send(response);
    } catch (err) {
      return handleError(err, res);
    }
  }

  @httpPost('/setToPost', passport.authenticate('jwt', { session: false }))
  async setCategoryToPost(req: AuthenticatedRequest, res: Response) {
    try {
      const response = await handleSetPostCategory(
        req,
        res,
        this.categoryService,
        this.postService
      );

      res.send(response);
    } catch (err) {
      return handleError(err, res);
    }
  }
}
