import passport from 'passport';
import { Response } from 'express';
import {
  controller,
  httpPost,
  httpGet,
  httpDelete,
  httpPut
} from 'inversify-express-utils';
import { inject } from 'inversify';

import {
  handleAddPost,
  handleGetPostsByUserId,
  handleGetPostByPostId,
  handleSetLikeRequest,
  handleGetPostLikeStatusRequest,
  handleGetPostLikesRequest,
  handleGetPostsByCategoryId,
  handleDeletePostRequest,
  handleEditPost,
  handleSetPostAsUserFavourite
} from '@controllers/PostsController';
import { handleGetTags } from '@controllers/TagsController';
import TYPES from '@helpers/DI/Types';
import IPostService from '@services/Post/IPostService';
import ITagService from '@services/Tag/ITagService';
import { ROUTES } from '@constants';
import IAuthorizationService from '@services/Authorization/IAuthorizationService';
import { AuthenticatedRequest } from '@models/User/AuthenticatedRequest';
import { handleError } from './utils';

@controller(`/${ROUTES.API_VERSION.V1}/posts`)
export class PostRoute {
  constructor(
    @inject(TYPES.IPostService) private postService: IPostService,
    @inject(TYPES.IAuthorizationService)
    private authorizationService: IAuthorizationService,
    @inject(TYPES.ITagService) private tagService: ITagService
  ) {}

  // @httpGet('/')
  // async getAllPosts(req: AuthenticatedRequest, res: Response) {
  //     try {
  //         const response = await handleGetAllPosts(req, res, this.postService);

  //         res.send(response);
  //     } catch (err) {
  //         return handleError(err, res)
  //     }
  // }

  @httpGet('')
  async getPostByCategoryId(req: AuthenticatedRequest, res: Response) {
    try {
      const response = await handleGetPostsByCategoryId(
        req,
        res,
        this.postService
      );

      res.send(response);
    } catch (err) {
      return handleError(err, res);
    }
  }

  @httpGet('/tags')
  async getTags(req: AuthenticatedRequest, res: Response) {
    try {
      const response = await handleGetTags(req, res, this.tagService);

      res.send({ tags: response });
    } catch (err) {
      res.send({ errorMessage: 'Something went wrong when retrieving tags' });
    }
  }

  @httpGet('/like/:postId')
  async getPostLikeStatus(req: AuthenticatedRequest, res: Response) {
    try {
      const response = await handleGetPostLikeStatusRequest(
        req,
        res,
        this.postService
      );

      res.send(response);
    } catch (err) {
      return handleError(err, res);
    }
  }

  @httpGet('/likes/:postId')
  async getPostLikes(req: AuthenticatedRequest, res: Response) {
    try {
      const response = await handleGetPostLikesRequest(
        req,
        res,
        this.postService
      );

      res.send(response);
    } catch (err) {
      return handleError(err, res);
    }
  }

  @httpGet('/user/:userId', passport.authenticate('jwt', { session: false }))
  async getPostsByUserId(req: AuthenticatedRequest, res: Response) {
    try {
      const response = await handleGetPostsByUserId(req, res, this.postService);

      res.send(response);
    } catch (err) {
      return handleError(err, res);
    }
  }

  @httpGet('/:postId')
  async getPostById(req: AuthenticatedRequest, res: Response) {
    try {
      const response = await handleGetPostByPostId(req, res, this.postService);

      res.send(response);
    } catch (err) {
      return handleError(err, res);
    }
  }

  @httpPost('/like', passport.authenticate('jwt', { session: false }))
  async likePost(req: AuthenticatedRequest, res: Response) {
    try {
      const response = await handleSetLikeRequest(req, res, this.postService);

      res.send(response);
    } catch (err) {
      return handleError(err, res);
    }
  }

  @httpPost(
    '/user/favourites',
    passport.authenticate('jwt', { session: false })
  )
  async setPostAsUserFavourite(req: AuthenticatedRequest, res: Response) {
    try {
      const response = await handleSetPostAsUserFavourite(
        +req.body.postId,
        +req.user.id,
        this.postService
      );

      res.send(response);
    } catch (err) {
      return handleError(err, res);
    }
  }

  @httpPut('/:postId', passport.authenticate('jwt', { session: false }))
  async editPostWithId(req: AuthenticatedRequest, res: Response) {
    try {
      const postId = await handleEditPost(req, res, this.postService);

      res.send({ postId });
    } catch (err) {
      return handleError(err, res);
    }
  }

  @httpPost('/add', passport.authenticate('jwt', { session: false }))
  async addPost(req: AuthenticatedRequest, res: Response) {
    try {
      const response = await handleAddPost(
        req,
        this.postService,
        this.authorizationService
      );

      res.status(201).send(response);
    } catch (err) {
      return handleError(err, res);
    }
  }

  @httpDelete('/:postId', passport.authenticate('jwt', { session: false }))
  async deletePostById(req: AuthenticatedRequest, res: Response) {
    try {
      const response = await handleDeletePostRequest(
        req,
        res,
        this.postService
      );

      res.send(response);
    } catch (err) {
      return handleError(err, res);
    }
  }
}
