import 'reflect-metadata';
import { InversifyExpressServer } from 'inversify-express-utils';
import { Container } from 'inversify';
import { CreateContainer } from './Helpers/DI/DI';
import TYPES from './Helpers/DI/Types';
import express, { Application } from 'express';
import expressSession from 'express-session';
import { urlencoded, json } from 'body-parser';
import cors from 'cors';
import passport from 'passport';
import swaggerUI from 'swagger-ui-express';

import { passportFactory } from '@helpers/Passport/passport';
import { apiV1, apiV2 } from './swagger';

import './WebApi/Routes';

const port = process.env.PORT || 3001;
const rootPath = `/api`;

const isDev = process.env.NODE_ENV === 'development';
class App {
  public server: InversifyExpressServer;

  constructor() {
    const container = CreateContainer();
    this.server = new InversifyExpressServer(container, null, {
      rootPath: rootPath
    });
    this.server.setConfig((app: Application) => {
      this.coreMiddleware(app);
      this.passportMiddleware(app, container);
      if (isDev) {
        app.use('/api-docs/v1', swaggerUI.serve, swaggerUI.setup(apiV1));
        app.use('/api-docs/v2', swaggerUI.serve, swaggerUI.setup(apiV2));
      }
      this.setRoutes(app);
    });

    const serverInstance = this.server.build();

    serverInstance.listen(port, () => {
      console.log(`Blog api v1.0.0 running on port: ${port}`);
    });
  }

  private coreMiddleware(app: Application): void {
    const options: cors.CorsOptions = {
      allowedHeaders: [
        'Origin',
        'X-Requested-With',
        'Content-Type',
        'Accept',
        'X-Access-Token',
        'Authorization'
      ],
      credentials: true,
      methods: 'GET,HEAD,OPTIONS,PUT,PATCH,POST,DELETE',
      origin: process.env.APPLICATION_URL,
      preflightContinue: false
    };

    app.use(cors(options));
    app.use(urlencoded({ extended: true }));
    app.use(json({ limit: '1.5mb' }), (err, req, res, next) => {
      if (err)
        return res
          .status(err.status)
          .json({ errorMessage: err.message, errorStackTrace: err.code });
      next(); // if it's not a 413, let the default error handling do it.
    });
    app.use(rootPath, express.static('uploads'));
    app.use('/images', express.static(__dirname + '/src/Assets/Images'));
  }

  private passportMiddleware(app: Application, container: Container) {
    app.use(
      expressSession({
        secret: process.env.EXPRESS_SESSION_SECRET,
        resave: true,
        saveUninitialized: true
      })
    );
    app.use(passport.initialize());
    app.use(passport.session());
    const passportStrategy = passportFactory(
      container.get(TYPES.IUserRepository)
    );
    passportStrategy(passport);
  }

  private setRoutes(app: Application): void {
    app.get('/', (_, res) => {
      if (isDev) {
        res.redirect('/api-docs/v1');
      } else {
        res.send('BLOG API');
      }
    });
  }
}

export default new App();
