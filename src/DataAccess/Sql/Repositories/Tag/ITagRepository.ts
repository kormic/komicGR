import Tag from '@models/Tag/Tag';

interface ITagRepository {
  getTags(): Promise<Tag[]>;
}

export default ITagRepository;
