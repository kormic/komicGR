import { inject, injectable } from 'inversify';

import TYPES from '@helpers/DI/Types';
import Tag from '@models/Tag/Tag';
import IUnitOfWork from '../../../../UnitOfWork/IUnitOfWork';
import ITagRepository from './ITagRepository';

@injectable()
class TagRepository implements ITagRepository {
  constructor(@inject(TYPES.IUnitOfWork) private uow: IUnitOfWork) {}

  async getTags(): Promise<Tag[]> {
    const sql = 'SELECT * from tags';

    const results: Tag[] = await this.uow.query(sql);

    const tags: Tag[] = [];
    for (const result of results) {
      const tag = new Tag(result.id, result.name);
      tags.push(tag);
    }

    return tags;
  }
}

export default TagRepository;
