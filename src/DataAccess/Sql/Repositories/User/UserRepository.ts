import * as mysql from 'mysql';
import { injectable, inject } from 'inversify';

import User from '@models/User/User';
import RegisterUserDTO from '@dtos/User/RegisterUserDTO';
import { QueryResponse } from '@dtos/QueryResponse';
import TYPES from '@helpers/DI/Types';
import BcryptService from '@helpers/Bcryptjs/BcryptService';
import IUserRepository from './IUserRepository';
import IUnitOfWork from '../../../../UnitOfWork/IUnitOfWork';

@injectable()
class UserRepository implements IUserRepository {
  constructor(
    @inject(TYPES.IUnitOfWork) private uow: IUnitOfWork,
    @inject(TYPES.BcryptService) private bcryptService: BcryptService
  ) {}

  getAll(): Promise<User[]> {
    let sql = 'SELECT * FROM users';
    const inserts = [];
    sql = mysql.format(sql, inserts);

    return this.uow.query(sql).then((result) => <User[]>result);
  }

  getById(id: number): Promise<User> {
    let sql = `SELECT * FROM users WHERE id=? limit 1`;
    const inserts = [id];
    sql = mysql.format(sql, inserts);

    return this.uow.query(sql).then((result) => <User>result[0]);
  }

  getByUsername(username: string): Promise<User> {
    let sql = `SELECT * FROM users WHERE username=? limit 1`;
    const inserts = [username];
    sql = mysql.format(sql, inserts);

    return this.uow
      .query(sql)
      .then((result) => <User>result[0])
      .catch((err) => {
        throw err;
      });
  }

  getByEmail(email: string): Promise<User> {
    let sql = `SELECT * FROM users WHERE email=? limit 1`;
    const inserts = [email];
    sql = mysql.format(sql, inserts);

    return this.uow
      .query(sql)
      .then((result) => <User>result[0])
      .catch((err) => {
        throw err;
      });
  }

  getUserByPostId(postId: number): Promise<User> {
    return this.uow
      .beginTransaction()
      .then(() => this.getPostUserIdQuery(postId))
      .then((query) => this.uow.query(query))
      .then((result) => {
        let sql = `SELECT * FROM users WHERE id=? limit 1`;
        const inserts = [result[0]['user_id']];
        sql = mysql.format(sql, inserts);

        return this.uow.query(sql);
      })
      .then((result) => {
        this.uow.commit();

        return <User>result[0];
      })
      .catch((errorFromSql) => {
        this.uow.rollback();
        throw errorFromSql;
      });
  }

  async insertUser(user: RegisterUserDTO): Promise<QueryResponse> {
    let sql = `INSERT INTO users (first_name, last_name, email, username, password, job_desc, address, mobile, registration_date, confirmed)
        values(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)`;
    const inserts = [
      user.first_name,
      user.last_name,
      user.email,
      user.username,
      user.password,
      user.job_desc,
      user.address,
      user.mobile,
      new Date(),
      false
    ];
    sql = mysql.format(sql, inserts);

    const result = await this.uow.query(sql);

    return result;
  }

  setConfirmedEmail(userId: number, confirmed: boolean): Promise<boolean> {
    let sql = `UPDATE users SET confirmed = ?  WHERE Id = ?`;
    const inserts = [confirmed, userId];
    sql = mysql.format(sql, inserts);

    return this.uow
      .query(sql)
      .then(() => true)
      .catch(() => {
        throw false;
      });
  }

  setProfileImageUrl(
    userId: number,
    profileImageUrl: string
  ): Promise<boolean> {
    let sql = `UPDATE users SET profile_image_url = ?  WHERE id = ?`;
    const inserts = [profileImageUrl, userId];
    sql = mysql.format(sql, inserts);

    return this.uow
      .query(sql)
      .then(() => true)
      .catch(() => {
        throw false;
      });
  }

  async updatePassword(userId: number, newPassword: string): Promise<boolean> {
    // TODO: Move the encryption of the password in the user service
    const salt = await BcryptService.generateSalt(10);
    const hashKey = await BcryptService.createHashKey(newPassword, salt);
    let sql = `UPDATE users SET password = ?  WHERE id = ?`;
    const inserts = [hashKey, userId];
    sql = mysql.format(sql, inserts);

    const result = await this.uow.query(sql);

    return result;
  }

  private getPostUserIdQuery(postId: number): string {
    let sql = `SELECT user_id FROM posts WHERE id=? limit 1`;
    const inserts = [postId];
    sql = mysql.format(sql, inserts);
    return sql;
  }
}

export default UserRepository;
