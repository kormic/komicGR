import * as mysql from 'mysql';
import { injectable, inject } from 'inversify';

import ICategoryRepository from './ICategoryRepository';
import AddCategoryDTO from '@dtos/Category/AddCategoryDTO';
import Category from '@models/Category/Category';
import TYPES from '@helpers/DI/Types';
import IUnitOfWork from '../../../../UnitOfWork/IUnitOfWork';

@injectable()
class CategoryRepository implements ICategoryRepository {
  constructor(@inject(TYPES.IUnitOfWork) private uow: IUnitOfWork) {}

  async addCategory(newCategory: AddCategoryDTO): Promise<any> {
    const sql = 'INSERT INTO Categories (name, description) values(?,?)';
    const inserts = [newCategory.Name, newCategory.Description];
    const formattedSql = mysql.format(sql, inserts);

    const result = await this.uow.query(formattedSql);

    return result;
  }

  setPostCategory(postId: number, categoryId: number): Promise<any> {
    let sql =
      'INSERT INTO `posts_categories` (`post_id`, `category_id`) values(?,?) ON DUPLICATE KEY UPDATE `category_id` = ?';
    const inserts = [postId, categoryId, categoryId];
    sql = mysql.format(sql, inserts);

    return this.uow
      .query(sql)
      .then((result) => result)
      .catch((err) => {
        throw err;
      });
  }

  getPostCategories(): Promise<Category[]> {
    let sql = 'SELECT c.id, c.name, c.description FROM `categories` c';
    const inserts = [];
    sql = mysql.format(sql, inserts);

    return this.uow
      .query(sql)
      .then((result) => <Category[]>result)
      .catch((err) => {
        throw err;
      });
  }
}

export default CategoryRepository;
