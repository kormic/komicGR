import AddCategoryDTO from '@dtos/Category/AddCategoryDTO';
import Category from '@models/Category/Category';

interface ICategoryRepository {
  addCategory(newCategory: AddCategoryDTO): Promise<any>;
  setPostCategory(postId: number, categoryId: number): Promise<any>;
  getPostCategories(): Promise<Category[]>;
}

export default ICategoryRepository;
